<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once("Secure_Controller.php");

/**
 * 
 */
class Mpesaadmin extends Secure_Controller
{
	public function __construct(){
		parent::__construct();
	    $this->load->helper('url');
	    $this->load->model('MpesaTransactions');
	    $this->load->model('MpesaShops');
	}

  public function saveusers(){

    $username = $this->input->post('username');
    $shop = $this->input->post('shop');
    $data = $this->input->post();

    for ($i=0; $i < count($username); $i++) { 
      $val = $username[$i];
      $imp = implode(",", $shop[$i]);
      $val1 = json_encode($imp);
      $this->MpesaShops->updateuser($val, $val1);
    }

    redirect('/mpesaadmin');
  }

	public function employees(){
		$employees = $this->MpesaTransactions->getEmployees();
		$draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));
		$data = array();
          foreach ($employees as $key => $v) {
             $data[] = array(
                $v->username
             );
          }
          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $data->num_rows,
                 "recordsFiltered" => $data->num_rows,
                 "data" => $data
            );
    	echo json_encode($output);
    	exit();
	}

	function index(){
		$employees['data'] = $this->MpesaTransactions->getEmployees();
    $employees['shops'] = $this->MpesaShops->getAll();

		$this->load->view('mpesaadmin/manage', $employees);
	}

  function Credits(){
    $data['credits'] = $this->Credits->Credits();
    echo json_encode($output);
    exit();
  }

	public function float(){
		$draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));
		$float = $this->MpesaTransactions->getFloat();
		$data = array();
          foreach ($float as $key => $v) {
             $data[] = array(
                $v->shop,
                $v->shop_name,
                number_format($v->cash, 2),
                number_format($v->float, 2)
             );
          }
          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $data->num_rows,
                 "recordsFiltered" => $data->num_rows,
                 "data" => $data
            );
    	echo json_encode($output);
    	exit();
	}

	public function edit($id){
	    $data = $this->MpesaTransactions->getFloat_by_id($id);
	    echo json_encode($data);
	  }

	public function getfloat(){
		$float = $this->MpesaTransactions->get_last_ten_entries();
		echo "string";
	}

  public function updatefloat()
  {
   $data = array(
      'id' => $this->input->post('id'),
          'cash' => $this->input->post('cash'),
          'float' => $this->input->post('float')
      );
    $update = $this->MpesaShops->updatefloat(array('shop' => $this->input->post('id')), $data);
    echo json_encode($update);
  }
}