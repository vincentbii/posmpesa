<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once("Secure_Controller.php");

class Mpesa extends Secure_Controller
{
	
	public function __construct()
	{
		parent::__construct();
    $this->load->helper('url');
    $this->load->model('MpesaTransactions');
    $this->load->model('MpesaShops');
	}

	public function index(){
    $shops = $this->MpesaShops->getShops($this->Employee->get_logged_in_employee_info()->username);
    // $this->load->view('mpesa/manage', $Employee);
    $sp = explode(",", json_decode($shops));
    $float = '';
    foreach ($sp as $key => $value) {
        $flo = json_encode($this->MpesaShops->getShopFloat($sp[$key]), TRUE);
        $float = $float.'#'.$flo;
    }
    $data['float'] = $float;
    // echo $float;
    $this->load->view('mpesa/manage', $data);
	}

  public function update(){
    $data = array(
      'id' => $this->input->post('id'),
        'transac_type' => $this->input->post('transac_type'),
          'amount' => $this->input->post('amount'),
          'cust_id' => $this->input->post('cust_id'),
          'shop' => $this->input->post('shop'),
          'phone_number' => $this->input->post('phone_number')
      );
    $this->MpesaTransactions->update(array('id' => $this->input->post('id')), $data);
    echo json_encode(array("status" => TRUE));
  }

  public function addtransaction(){
    $data = array(
        'transac_type' => $this->input->post('transac_type'),
          'phone_number' => $this->input->post('phone_number'),
          'amount' => $this->input->post('amount'),
          'cust_id' => $this->input->post('cust_id'),
          'shop' => $this->input->post('shop'),
          'user_id' => $this->Employee->get_logged_in_employee_info()->person_id,
      );
    $shop = $this->input->post('shop');
    $cash = $this->input->post('amount');
    $transac_type = $this->input->post('transac_type');

    $updateF = $this->MpesaTransactions->updateFloat($shop, $cash, $transac_type);
    $insert = $this->MpesaTransactions->addTransaction($data);
    echo json_encode($updateF);
    // echo $updateF;
  }

  public function edit($id){
    $data = $this->MpesaTransactions->get_by_id($id);
    echo json_encode($data);
  }

  public function shops(){
    $data = $this->MpesaShops->getAll();
    header('Content-Type: application/json');
    echo json_encode($data);
  }

  public function Float(){
    
  }

	public function transactions(){
			$draw = intval($this->input->get("draw"));
      $start = intval($this->input->get("start"));
      $length = intval($this->input->get("length"));
      $trans = $this->MpesaTransactions->get_last_ten_entries($this->Employee->get_logged_in_employee_info()->person_id);

      $data = array();
      foreach ($trans as $key => $v) {
         $data[] = array(
            $v->id,
            $v->type_name,
            $v->shop_name,
            $v->transact_date,
            $v->first_name.' '.$v->last_name,
            $v->phone_number,
            number_format($v->amount, 2),
            $v->cust_id
         );
      }
      $output = array(
           "draw" => $draw,
             "recordsTotal" => $data->num_rows,
             "recordsFiltered" => $data->num_rows,
             "data" => $data
        );
      echo json_encode($output);
      exit();
          // echo "Hello, World";
	}

  public function delete(){
    $this->MpesaTransactions->delete_by_id($id);
    echo json_encode(array("status" => TRUE));
  }

	public function test(){
		echo "Hello, World";
	}
}

?>