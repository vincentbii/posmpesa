<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once("Secure_Controller.php");

class Credits_admin extends Secure_Controller
{
	public function __construct()
	{
		parent::__construct('credits');
		$this->load->model('Credit');
	}

	public function index()
	{
		$this->load->view('mpesaadmin/creditsadmin');
	}

	public function edit($id)
	{
		$data = $this->Credit->editCredit($id);
	    echo json_encode($data);
	}

	public function getCredit()
	{
		$credits = $this->Credit->getCredit();
		$data = array();
	      foreach ($credits as $key => $v) {
	      	$qty = $this->Credit->getDenQty($v->id);
	         $data[] = array(
	            'id' => $v->id,
	            'denomination' => $v->denomination,
	            'qty' => $qty->total,
	            'wprice' => $v->wholesale_price,
	            'rprice' => $v->retail_price,
	            'bprice' => $v->buying_price
	         );
	      }

	      $output = [
	      	'data' => $data
	      ];
	    header('Content-Type: application/json');
	    echo json_encode( $output );
	}

	public function getalltransactions()
	{
		$credits['data'] = $this->Credit->allTransactions();
		header('Content-Type: application/json');
	    echo json_encode( $credits );
	}

	public function getTransactionsSummary()
	{
		$credits['data'] = $this->Credit->transactionsSummary();
		header('Content-Type: application/json');
	    echo json_encode( $credits );
	}

	public function update()
	{
		$data = array(
	          'selling_price' => $this->input->post('selling_price'),
	          'buying_price' => $this->input->post('buying_price')
	      );
	    $update = $this->Credit->update($this->input->post('id'), $data);
	    echo json_encode("Updated Successfully!");
	}

}
?>
