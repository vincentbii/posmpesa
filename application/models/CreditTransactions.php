<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Item class
 */

class CreditTransactions extends CI_Model
{

	var $credit_table = 'ospos_credit_transactions';

	/*
	Determines if a given item_id is an item
	*/
	public function getCreditTransactions(){
		$person = $this->Employee->get_logged_in_employee_info()->person_id;
		$query = $this->db->query("SELECT a.id, b.denomination, a.quantity, date_format(a.`datetime`, '%d-%m-%Y') as datetime, a.amount, c.first_name, c.last_name, t.credit_type FROM ospos_credit_transactions AS a INNER JOIN ospos_credit_denominations AS b ON a.denomination = b.id INNER JOIN ospos_people AS c ON a.user_id = c.person_id inner join ospos_credit_type as t on a.type = t.id WHERE c.person_id = '$person'");
        return $query->result();
	}

	public function edit($id){
		$query = $this->db->query("SELECT a.id, a.amount, b.credit, b.id as cid FROM ospos_credit_transactions as A INNER JOIN ospos_credits AS b On a.credit = b.id WHERE a.id='$id'");
        return $query->row_array();
	}

	public function addTransactions($data)
	{
		$query = $this->db->insert('ospos_credit_transactions', $data);
		if($query){
			return "Inserted Successfully!";
		}
	}

	public function addReceivables($data)
	{
		// $query = $this->db->insert('ospos_credits_received_packets', $data);
		$query = $this->db->insert('ospos_credits_received_packets', $data);
		$insert_id = $this->db->insert_id();
		if($query){
			return $insert_id;
		}
	}

	public function addTransaction($data)
	{
		$query = $this->db->insert('ospos_credit_transactions', $data);
		if($query){
			return true;
		}else{
			return false;
		}
	}

	public function update($id, $data)
	{
		$this->db->where('id', $id);
		$query = $this->db->update('ospos_credit_transactions', $data);
		if($query){
			return true;
		}else{
			return false;
		}
	}
}
?>
