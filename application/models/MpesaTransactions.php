<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class MpesaTransactions extends CI_Model
{
        var $table = 'ospos_mpesa_transactions';
        var $floatTable = 'ospos_mpesa_float';

        public function __construct()
        {
                parent::__construct();
                $this->load->database();
        }

        public function floatt(){
            $query = $this->db->get($this->$floatTable);
            return $query->result();
        }

        public function getFloat(){
            $query = $this->db->query("SELECT f.cash, f.float, f.shop, s.shop_name FROM ospos_mpesa_float AS f INNER JOIN ospos_mpesa_shop AS s ON f.shop = s.id");
            return $query->result();
        }

        function CF($id){
            $yesterday = date("Y-m-d", strtotime("yesterday"));
            $deposits = $this->db->query("SELECT SUM(amount) as total FROM ospos_mpesa_transactions WHERE transac_type = 1 AND shop = '$id' AND DATE_FORMAT(transact_date, '%Y-%m-%d') = '$yesterday'");
            $withdrawals = $this->db->query("SELECT SUM(amount) as total FROM ospos_mpesa_transactions WHERE transac_type = 2 AND shop = '$id' AND DATE_FORMAT(transact_date, '%Y-%m-%d') = '$yesterday'");

            $row = $deposits->row(0);
            $row1 = $withdrawals->row(0);

            $dif = $row1->total - $row->total;

            return $dif; 
        }
	
	public function get_last_ten_entries($id)
        {
                $this->db->select('ospos_mpesa_transactions.phone_number, ospos_mpesa_transactions.cust_id, ospos_mpesa_transactions.amount, ospos_mpesa_transactions.id, ospos_people.first_name, ospos_people.last_name, ospos_mpesa_shop.shop_name, ospos_mpesa_transaction_type.type_name, ospos_mpesa_transactions.transact_date');
                $this->db->from($this->table);
                $this->db->join('ospos_mpesa_transaction_type', 'ospos_mpesa_transaction_type.id = ospos_mpesa_transactions.transac_type');
                $this->db->join('ospos_mpesa_shop', 'ospos_mpesa_shop.id = ospos_mpesa_transactions.shop');
                $this->db->join('ospos_people', 'ospos_people.person_id = ospos_mpesa_transactions.user_id');
                $this->db->where('ospos_people.person_id', $id);
                 
                $q = $this->db->get();
                    if($q->num_rows() > 0)
                    {
                      foreach ($q->result() as $row)
                      {
                        $data[] = $row;
                      }
                      return $data;
                    }
                
        }

        public function update($where, $data){
                $this->db->update($this->table, $data, $where);
                return $this->db->affected_rows();
        }

        public function getEmployees(){
            $query = $this->db->query("SELECT e.person_id, e.username, p.first_name, p.last_name from ospos_employees e inner join ospos_people p on e.person_id = p.person_id");
            return $query->result();
          }


        public function delete_by_id($id){
                $this->db->where('id', $id);
                $this->db->delete($this->table);
        }

        public function get_by_id($id){
            $this->db->select('ospos_mpesa_shop.shop_name, ospos_mpesa_shop.shop_code, ospos_mpesa_transactions.shop, ospos_mpesa_transactions.id, ospos_mpesa_transactions.cust_id,ospos_mpesa_transactions.transac_type, ospos_mpesa_transactions.amount, ospos_mpesa_transactions.user_id, ospos_mpesa_transactions.phone_number');
            $this->db->from($this->table);
            $this->db->join('ospos_mpesa_shop', 'ospos_mpesa_shop.id = ospos_mpesa_transactions.shop');
            $this->db->where('ospos_mpesa_transactions.id',$id);
            $query = $this->db->get();

            return $query->row();
        }

        public function getFloat_by_id($id){

            $query = $this->db->query("SELECT f.cash, f.float, s.id, s.shop_name FROM ospos_mpesa_float AS f INNER JOIN ospos_mpesa_shop AS s ON f.shop = s.id WHERE f.shop = '$id'");
            return $query->row_array();

            // return $query->row();
        }

        public function updateFloat($shop, $cash, $transac_type)
        {
            if($transac_type == 1){
                $query = $this->db->query("UPDATE ospos_mpesa_float SET `float` = `float` - '$cash', cash = cash + '$cash' WHERE shop = '$shop'");
            }elseif ($transac_type == 2) {
                $query = $this->db->query("UPDATE ospos_mpesa_float SET `float` = `float` + '$cash', cash = cash - '$cash' WHERE shop = '$shop'");
            }
            if($query){
                return TRUE;
            }else{
                return FALSE;
            }
        }

        public function addTransaction($data)
        {
                $this->db->insert($this->table, $data);
                return $this->db->insert_id();
        }

        public function update_entry()
        {
                $this->title    = $_POST['title'];
                $this->content  = $_POST['content'];
                $this->date     = time();

                $this->db->update('entries', $this, array('id' => $_POST['id']));
        }
}