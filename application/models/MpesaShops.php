<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 */
class MpesaShops extends CI_Model
{
	
	var $table = 'ospos_mpesa_shop';

        public function __construct()
        {
                parent::__construct();
                $this->load->database();
        }
	
	     public function getAll(){
                $this->db->select('*');
                $this->db->from($this->table);
                $this->db->where(array('status' => 1));
                 
                $q = $this->db->get();
                    if($q->num_rows() > 0)
                    {
                      foreach ($q->result() as $row)
                      {
                        $data[] = $row;
                      }
                      return $data;
                    }
                
        }

        public function updatefloat($where, $data){
                $this->db->update('ospos_mpesa_float', $data, $where);
                return $this->db->affected_rows();
        }

        public function getShops($person_id)
        {
          $query = $this->db->query("SELECT shop from ospos_mpesa_shop_user WHERE user = '$person_id'");
          $row = $query->row(0);
          return $row->shop;
        }

        public function getShopFloat($sp)
        {
          $query1 = $this->db->query("SELECT f.cash, f.`float`, s.shop_name  from ospos_mpesa_float AS f INNER JOIN ospos_mpesa_shop AS s ON f.shop = s.id WHERE shop = '$sp'");
          $floatRow = $query1->row_array();
          return $floatRow;
        }

        public function mpesaShops(){
          $query = $this->db->query("SELECT * FROM ospos_mpesa_shop WHERE status =1");
          return $query->result();
        }

        public function updateuser($user, $shop)
        {
          $this->db->where('user', $user);
          $q = $this->db->get('ospos_mpesa_shop_user');
          $this->db->reset_query();
            
          if ( $q->num_rows() > 0 ) 
          {
            $query = $this->db->query("UPDATE ospos_mpesa_shop_user SET shop = '$shop' WHERE user = '$user'");
          } else {
            $query = $this->db->query("INSERT into ospos_mpesa_shop_user (user, shop) values ('$user', $shop)");
          }
        }
}