<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class MpesaTransacTypes extends CI_Model
{
	
	function __construct()
	{
		# code...
	}

	public function get_last_ten_entries(){
        $query = $this->db->get('mpesa_transaction_type', 10);
        return $query->result();
    }
}