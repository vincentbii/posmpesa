<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Item class
 */

class Credit extends CI_Model
{

	var $credit_table = 'ospos_credits';
	

	/*
	Determines if a given item_id is an item
	*/
	public function getCredit(){
		$query = $this->db->query("SELECT b.id, b.denomination, b.wholesale_price, b.retail_price, b.buying_price FROM ospos_credit_denominations AS b  WHERE b.status = 1");
        return $query->result();
	}

	public function saveIssued($shop, $id, $sno, $denomination)
	{
		$person = $this->Employee->get_logged_in_employee_info()->person_id;
		$query2 = $this->db->query("SELECT cards_qty FROM ospos_credit_denominations WHERE denomination = '$denomination'");
		$row = $query2->row();
		$query = $this->db->query("UPDATE ospos_credits_received_packet_details set issued = 1, issued_by = '$person', shop = '$shop', card_qty = '".$row->cards_qty."' WHERE id = '$id'");
		if($query){
			return TRUE;
		}else{
			return false;
		}
	}

	public function getCreditDetails()
	{
		$query = $this->db->query("SELECT a.id, a.serial_number, c.denomination FROM ospos_credits_received_packet_details AS a INNER JOIN ospos_credits_received_packets AS b ON a.packet_id = b.id INNER JOIN ospos_credit_denominations as c ON b.denomination = c.id WHERE a.issued = 0");
        return $query->result();
	}

	public function saveRecDetails($sno, $id)
	{
		$query = $this->db->query("INSERT into ospos_credits_received_packet_details (serial_number, packet_id) VALUES ('$sno', $id)");
        return TRUE;
	}

	public function getReceivables()
	{
		$query = $this->db->query("SELECT a.id, a.serial_number, c.denomination FROM ospos_credits_received_packet_details as a inner join ospos_credits_received_packets as b on a.packet_id = b.id inner join ospos_credit_denominations as c on b.denomination = c.id");
        return $query->result();
	}

	public function getDenominations(){
		$query = $this->db->query("SELECT id, denomination from ospos_credit_denominations WHERE status = 1");
        return $query->result();
	}

	public function transactionsSummary()
	{
		$query = $this->db->query("SELECT DISTINCT credit from ospos_credits WHERE status = 1");
        return $query->result();
	}

	public function allTransactions()
	{
		$query = $this->db->query("SELECT a.id, b.credit, date_format(a.`datetime`, '%d-%m-%Y') as datetime, a.amount, c.first_name, c.last_name, a.status FROM ospos_credit_transactions AS a INNER JOIN ospos_credits AS b ON a.credit = b.id INNER JOIN ospos_people AS c ON a.user_id = c.person_id");
        return $query->result();
	}

	public function getDenQty($den)
	{
		$query = $this->db->query("SELECT SUM(qty_packets) as total FROM ospos_credits_received_packets WHERE denomination = '$den' and status = 1");
        return $query->row(0);
	}

	public function editCredit($id){
		$query = $this->db->query("SELECT a.id, a.credit, a.selling_price, a.buying_price FROM ospos_credits AS a WHERE a.status = 1 AND a.id = '$id'");
        return $query->row_array();
	}

	public function update($id, $data)
	{
		$this->db->where('id', $id);
		$query = $this->db->update('ospos_credits', $data);
		if($query){
			return true;
		}else{
			return false;
		}
	}
}
?>
