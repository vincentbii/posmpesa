<?php $this->load->view("partial/header"); ?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<div id="table_holder">
    <?php
    $data = explode("#", $float);
    foreach ($data as $key => $value) {
        if($value)
        $json = json_decode($value, true);
        ?>
        <tr>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
        </tr>
        <tr>
            <td><?php
             if($json['shop_name'] !== null){
                echo '<b>'.$json['shop_name'].'</b>'.'&nbsp;&nbsp;';
             }
              ?></td>
            <td><?php
            
            if($json['cash'] !== null){
                echo 'Cash at Hand: '.$json['cash'].'&nbsp;&nbsp;';    
            }
            ?></td>
            <td><?php
            
            if($json['float'] !== null){
                echo 'Float: '.$json['float'].'&nbsp;&nbsp;';
            }
            ?></td>
        </tr>
        <?php
    }
    ?>
    <table id="transactions_mpesa" class="display" style="width:100%">
        <thead>
            <tr>
                <th>#</th>
                <th>Transaction Type</th>
                <th>Shop</th>
                <th>Transaction Date</th>
                <th>User</th>
                <th>Phone Number</th>
                <th>Amount</th>
                <th>Customer ID</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
</div>
<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> -->
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript">
    var save_method;
    $(document).ready(function() {
        
    var table = $('#transactions_mpesa').DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                text: 'New',
                className: 'btn-success',
                action: function ( e, dt, node, config ) {
                    var table;
                    save_method = 'add';
                    $.ajax({  
                        url:"<?php echo site_url("mpesa/shops") ?>", 
                        method:"GET",  
                        dataType:"json",  
                        success:function(data){  
                            $('#form')[0].reset();
                            $('#modal_form').modal('show');  
                            $('select[name="shop"]').empty();
                            $.each(data, function(key, value) {
                                $('select[name="shop"]').append('<option value="'+ value.id +'">'+ value.shop_name + ' - ' + value.shop_code +'</option>');
                            });
                        }  
                   });
                }
            },
            {
                text: "Float",
                action: function(){

                }
            }
        ],
        "ajax": {
            url : "<?php echo site_url("mpesa/transactions") ?>",
            type : 'GET'
        },
        "columnDefs": [ {
          "targets": -1,
          "data": null,
          "defaultContent": 
             '<button class="btn btn-xs btn-primary btn-view" type="button">Edit</button> | '
             + '<button class="btn btn-xs btn-danger btn-delete"  type="button">Delete</button>'
        } ]
    });

    $('#transactions_mpesa tbody').on( 'click', 'button', function () {
        save_method = 'update';
        $('#form')[0].reset();
        var data = table.row( $(this).parents('tr') ).data();
        // alert( data[0] +"'s salary is: "+ data[ 5 ] );
        var id = data[0];

            $.ajax({
            url : "<?php echo site_url('mpesa/edit/')?>/" + data[0],
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
     
                $('[name="id"]').val(data.id);
                $('[name="cust_id"]').val(data.cust_id);
                $('[name="amount"]').val(data.amount);
                $('[name="shop_name"]').val(data.shop_name);
                $('[name="transac_type"]').val(data.transac_type);
                $('[name="phone_number"]').val(data.phone_number);
     
     
                $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('Edit Transaction'); // Set title to Bootstrap modal title

                $('select[name="shop"]').append('<option value="'+ data.shop +'">'+ data.shop_name + ' - ' + data.shop_code +'</option>');
     
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });


    } );

    // Handle click on "View" button
     // $('#transactions_mpesa tbody').on('click', '.btn-view', function (e) {
     //    var data = table.row( $(this).parents('tr') ).data();
     //    alert(data)
     // } );


     // Handle click on "Delete" button
     $('#example tbody').on('click', '.btn-delete', function (e) {
        //var data = table.row( $(this).parents('tr') ).data();
     } );
    
    } );
    function save(){
        var url;
          if(save_method == 'add')
          {
              url = "<?php echo site_url('mpesa/addTransaction')?>";
          }
          else
          {
            url = "<?php echo site_url('mpesa/update')?>";
          }

          $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               $('#modal_form').modal('hide');
               alert(data)
              location.reload();// for reload a page
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
        
    }
</script>
<!-- Bootstrap modal -->
  <div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Mpesa Transaction Form</h3>
      </div>
      <div class="modal-body form">
        <?php echo form_open('#', array('id'=>'form', 'enctype'=>'multipart/form-data', 'class'=>'form-horizontal')); ?>
            <input type="hidden" value="<?php echo $this->security->get_csrf_hash(); ?>" name="<?php echo $this->security->get_csrf_token_name(); ?>">
          <div class="form-body">
            <input type="hidden" name="id" value="">

            <?php
            $options = array(
                '1'         => 'Deposit',
                '2'         => 'Withdrawal',
            );
            ?>

            <div class="form-group form-group-sm">
                <?php echo form_label('Transaction Type', 'transac_type', array('class'=>'control-label col-xs-3', 'placeholder' => 'Transaction Type')); ?>
              <div class="col-xs-8">
                <?php echo form_dropdown('transac_type', $options, '', 'class="form-control"'); ?>
              </div>
            </div>

            <div class="form-group form-group-sm">
            <?php echo form_label('Phone Number', 'phone_number', array('class'=>'required control-label col-xs-3')); ?>
            <div class='col-xs-8'>
                <?php echo form_input(array(
                        'name'=>'phone_number',
                        'id'=>'phone_number',
                        'type' => 'number',
                        'class'=>'form-control input-sm')
                        );?>
            </div>
        </div>

        <div class="form-group form-group-sm">
            <!-- <div class=""> -->
                <?php echo form_label('Select Shop', 'shop', array('class'=>'required control-label col-xs-3', 'placeholder' => 'Shop')); ?>
            <!-- </div> -->
            <div class='col-xs-8'>
                <select name="shop" class="form-control" style="width:350px">
                </select>
            </div>
        </div>

            <div class="form-group form-group-sm">
            <?php echo form_label('Customer ID', 'cust_id', array('class'=>'required control-label col-xs-3')); ?>
            <div class='col-xs-8'>
                <?php echo form_input(array(
                        'name'=>'cust_id',
                        'id'=>'cust_id',
                        'type' => 'number',
                        'class'=>'form-control input-sm')
                        );?>
            </div>
        </div>
        <div class="form-group form-group-sm">
            <?php echo form_label('Amount', 'amount', array('class'=>'required control-label col-xs-3')); ?>
            <div class='col-xs-8'>
                <?php echo form_input(array(
                        'name'=>'amount',
                        'id'=>'amount',
                        'type' => 'number',
                        'class'=>'form-control input-sm')
                        );?>
            </div>
        </div>
 
          </div>
        <?php echo form_close(); ?>
          </div>
          <div class="modal-footer">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->

<?php $this->load->view("partial/footer"); ?>
