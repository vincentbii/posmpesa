<?php $this->load->view("partial/header"); ?>
<div class="row">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

<div class="container">
	<div class="row">
		<h3>Credit Transactions</h3>
		<table id="transactions" class="display">
			<thead>
				<tr>
					<th>#</th>
					<th>Denomination</th>
					<th>Quantity</th>
          <th>Transaction Type</th>
					<th>Transaction Date</th>
					<th>Action</th>
				</tr>
			</thead>
		</table>
	</div>
</div>

<?php $this->load->view("partial/footer"); ?>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
	    var table = $('#transactions').DataTable( {
	    	dom: 'Bfrtip',
        buttons: [
            {
                text: 'Make a Sale',
                className: 'btn-success',
                action: function ( e, dt, node, config ) {
                    var table;
                    save_method = 'add';
                    $.ajax({  
                        url:"<?php echo site_url("credits/getDenominations") ?>", 
                        method:"GET",  
                        dataType:"json",  
                        success:function(data){  
                            $('#form')[0].reset();
                            $('#modal_form').modal('show');
                            $('select[name="denomination"]').empty();
                            $.each(data, function(key, value) {
                                $('select[name="denomination"]').append('<option value="'+ value.id +'">'+ value.denomination +'</option>');
                            });
                        }  
                   });
                }
            },
            {
                text: "Float",
                action: function(){

                }
            }
        ],
	    	"ajax": "<?php echo site_url("credits/getTransactions") ?>",
	        "columns": [
	            { "data": "id" },
	            { "data": "denomination" },
              { "data": "credit_type" },
	            { "data": "datetime" },
	            { 
	            	"data": null,
	            	render: function (data, type, row) {
                        var details = row.last_name + " " + row.first_name;
                        return details;
                    }
                },
                { "data": {
	            	"targets": -1,
	            	"data": "id",
	            	"defaultContent": "<button class='btn btn-xs btn-success'><i class='glyphicon glyphicon-edit'></i></button>"
	            }}
	        ],
	        "columnDefs": [ {
	            	"targets": -1,
	            	"data": "id",
	            	"defaultContent": "<button class='btn btn-xs btn-success'><i class='glyphicon glyphicon-edit'></i></button>"
	        } ]
	    });

	    $('#transactions tbody').on( 'click', 'button', function () {
	    	save_method = 'update';
	    	$('#form')[0].reset();
        	var data = table.row( $(this).parents('tr') ).data();
        	// alert( data['id'] +"'s salary is: "+ data['credit'] );

        	$.ajax({
	            url : "<?php echo site_url('credits/edit/')?>/" + data['id'],
	            type: "GET",
	            dataType: "JSON",
	            success: function(data)
	            {
	                $('[name="id"]').val(data.id);
	                $('[name="amount"]').val(data.amount);
	                // $('select[name="credit"]').empty();
                    // $.each(data, function(key, value) {
                        $('select[name="credit"]').append('<option value="'+ data.cid +'">'+ data.credit +'</option>');
                    // });
	     
	                $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
	                $('.modal-title').text('Edit Credit Transaction'); // Set title to Bootstrap modal title
	     
	            },
	            error: function (jqXHR, textStatus, errorThrown)
	            {
	                alert('Error get data from ajax');
	            }
	        });
    	} );

	});

	function save(){
        var url;
          if(save_method == 'add')
          {
              url = "<?php echo site_url('credits/addTransactions')?>";
          }
          else
          {
            url = "<?php echo site_url('credits/update')?>";
          }

          $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               $('#modal_form').modal('hide');
               // window.location.href = '<?php echo site_url("credits"); ?>/' + data.id + '/' + data.qty;
               alert(data)
              // location.reload();// for reload a page
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
        
    }

</script>
<div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Credit Transaction Form</h3>
      </div>
      <div class="modal-body form">
        <?php echo form_open('#', array('id'=>'form', 'enctype'=>'multipart/form-data', 'class'=>'form-horizontal')); ?>
            <input type="hidden" value="<?php echo $this->security->get_csrf_hash(); ?>" name="<?php echo $this->security->get_csrf_token_name(); ?>">
          <div class="form-body">
            <input type="hidden" name="id" value="">
        <div class="form-group form-group-sm">
            <!-- <div class=""> -->
                <?php echo form_label('Denomination', 'denomination', array('class'=>'required control-label col-xs-3', 'placeholder' => 'Denomination')); ?>
            <!-- </div> -->
            <div class='col-xs-8'>
                <select name="denomination" class="form-control" style="width:350px">
                </select>
            </div>
        </div>

            <div class="form-group form-group-sm">
            <?php echo form_label('Quantity', 'quantity', array('class'=>'required control-label col-xs-3')); ?>
            <div class='col-xs-8'>
                <?php echo form_input(array(
                        'name'=>'quantity',
                        'id'=>'quantity',
                        'placeholder' => 'Quantity of Packets',
                        'type' => 'number',
                        'class'=>'form-control input-sm')
                        );?>
            </div>
        </div>

        <div class="form-group form-group-sm">
            <?php echo form_label('Transaction Type', 'ttype', array('class'=>'required control-label col-xs-3')); ?>
            <div class='col-xs-8'>
                <?php
                    $options = array(
                          '1'         => 'Retail',
                          '2'         => 'Wholesale',
                  );
                    $js = array(
                          'id'       => 'type',
                          'onChange' => '',
                          'class'    => 'form-control'
                  );

                  $ctypes = array('RT', 'WH');
                  echo form_dropdown('ttype', $options, 'RT', $js);    
                ?>
            </div>
        </div>
 
          </div>
        <?php echo form_close(); ?>
          </div>
          <div class="modal-footer">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Next</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->