<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.2.7/css/select.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<style type="text/css">
	#events {
        margin-bottom: 1em;
        padding: 1em;
        background-color: #f6f6f6;
        border: 1px solid #999;
        border-radius: 3px;
        height: 100px;
        overflow: auto;
    }
</style>
<?php $this->load->view("partial/header"); ?>
<div class="container">
	<div class="row">
		<table id="issueDetails" class="cell-border compact stripe" style="width:100%">
			<thead>
				<tr>
					<th>ID</th>
					<th>Denomination</th>
					<th>Serial Number</th>
				</tr>
			</thead>
		</table>
	</div>
</div>
<?php $this->load->view("partial/footer"); ?>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js"></script>
<script type="text/javascript">
	var events = $('#events');
	var oTable = $('#issueDetails').DataTable( {
		"select": {
            "style": 'multi'
        },
        "dom": 'Bfrtip',
          "buttons": [
              // {
              //     "text": 'Issue Items',
              //     "className": 'Issue_items',
              //     action: function ( e, dt, node, config ) {
              //         // window.location.href = "<?php echo site_url("credits/issueDetails"); ?>";
              //         alert("HI")
              //     }
              // },
              {
	                "text": 'Issue selected data',
	                action: function () {
	                    // alert( oTable.rows('.selected').data().length +' row(s) selected' );
	                    var arraySelected = [];
						   oTable.rows('.selected').every(function(rowIdx) {
						      arraySelected.push(oTable.row(rowIdx).data())
						   })   
						   console.log(arraySelected);
						   var jsonSelected = JSON.stringify(arraySelected);
						$.ajax({  
	                        url:"<?php echo site_url("mpesa/shops") ?>", 
	                        method:"GET",  
	                        success:function(data, array){  
	                            $('#form')[0].reset();
	                            $('[name="arraySelected"]').val(jsonSelected);
	                            $('#modal_form').modal('show');
	                            $('select[name="shop"]').empty();
	                            $.each(data, function(key, value) {
	                                $('select[name="shop"]').append('<option value="'+ value.id +'">'+ value.shop_name +'</option>');
	                            });
	                        }  
	                   });
	                }
	            }
          ],
	        "ajax": "<?php echo site_url("credits/creDetails") ?>",
	        "columns": [
	            { "data": "id" },
	            { "data": "denomination" },
              	{ "data": "serial_number" }
	        ],
	        "columnDefs": [ {
	            	"targets": -1,
	            	"data": "id",
	            	"defaultContent": "<button id='add_item' class='btn btn-xs btn-success'><i class='glyphicon glyphicon-plus'></i></button>"
	        } ]
	    } );

		$('#issueDetails tbody').on( 'click', 'tr', function () {
	        $(this).toggleClass('selected');
	    } );
	 
	    $('.Issue_items').click( function () {
	        alert( oTable.rows('.selected').data().length +' row(s) selected' );
	    } );

	    function save(){
        	var url;
          	url = "<?php echo site_url('credits/saveIssued'); ?>";

          $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            success: function(data)
            {
               //if success close modal and reload ajax table
               $('#modal_form').modal('hide');
               // window.location.href = '<?php echo site_url("credits"); ?>/' + data.id + '/' + data.qty;
               alert(data)
              location.reload();// for reload a page
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert(errorThrown+'Error adding / update data');
            }
        });
        
    }
</script>

<div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Issue Items</h3>
      </div>
      <div class="modal-body form">
        <?php echo form_open('#', array('id'=>'form', 'enctype'=>'multipart/form-data', 'class'=>'form-horizontal')); ?>
            <input type="hidden" value="<?php echo $this->security->get_csrf_hash(); ?>" name="<?php echo $this->security->get_csrf_token_name(); ?>">

            <input type="hidden" name="arraySelected">
          <div class="form-body">
        <div class="form-group form-group-sm">
            <?php echo form_label('Shop', 'shop', array('class'=>'required control-label col-xs-3')); ?>
            <div class='col-xs-8'>
                <select name="shop" class="form-control" style="width:350px">
                </select>
            </div>
        </div>
 
          </div>
        <?php echo form_close(); ?>
          </div>
          <div class="modal-footer">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->