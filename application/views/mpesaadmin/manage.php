<?php $this->load->view("partial/header"); ?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<div class="row">
	<h2>Float Management</h2>
	<table class="table-condensed table-striped" id="float">
		<thead>
			<tr>
				<th>#</th>
				<th>Shop</th>
				<th>Cash</th>
				<th>Float</th>
				<th>Action</th>
			</tr>
		</thead>
	</table>
</div>
<div class="row" class="table-condensed">
	<h2>User - Shops Management</h2>
	<table class="table-condensed table-striped table">
		<thead>
			<tr>
				<th>User</th>
				<th>Shops</th>
			</tr>
		</thead>
		<tbody>
			<?php echo form_open('mpesaadmin/saveusers/', array('id'=>'users_form', 'enctype'=>'multipart/form-data', 'class'=>'form-horizontal')); ?>
			<?php foreach ($data as $key => $value) { ?>
				<tr>
					<td>
						<?php echo form_input(array(
						'name'=>'username[]',
						'id'=>'username',
						'class'=>'form-control input-sm',
						'value'=>$value->username,
						'size' => '1'
					)
						);?>
					</td>

					<?php $k = 0; foreach ($shops as $k => $s) { ?>
						<td>
							<?php
							$k = $k + 1;
							$ckbdata = array(
						        'name'          => "shop[$key][]",
						        'id'            => 'shop',
						        'value'         => $s->id,
						        'checked'       => FALSE,
						        'style'         => 'margin:10px'
							);

							echo form_checkbox($ckbdata);
							echo form_label($s->shop_name, $s->id);
							?>
						</td>
					<?php } ?>
				</tr>
				
			<?php } ?>

			<tr>
				<?php $colspan = $k + 1; ?>
				<td style="text-align: right;" colspan="<?php $colspan; ?>">
					<?php
						$databtnr = array(
					        'name'          => 'button',
					        'id'            => 'button',
					        'value'         => 'true',
					        'type'          => 'reset',
					        'content'       => 'Reset',
					        'class'			=> 'btn btn-primary btn-xs'
						);

						echo form_button($databtnr);

						$databtns = array(
					        'name'          => 'button',
					        'id'            => 'button',
					        'value'         => 'true',
					        'type'          => 'submit',
					        'content'       => 'Submit',
					        'class'			=> 'btn btn-info btn-xs'
						);

						echo form_button($databtns);
					?>
				</td>
			</tr>

			<?php echo form_close(); ?>
		</tbody>
	</table>
</div>

<?php $this->load->view("partial/footer"); ?>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		var table = $('#users').DataTable({
			"ajax": {
	        	url : "<?php echo site_url("mpesaadmin/employees") ?>",
            	type : 'GET'
	        }
		});
	});
	$(document).ready(function() {
	    var table = $('#float').DataTable( {
	        "ajax": {
	        	url : "<?php echo site_url("mpesaadmin/float") ?>",
            	type : 'GET'
	        },
	        "columnDefs": [ {
	          "targets": -1,
	          "data": null,
	          "defaultContent": 
	             '<button class="btn btn-xs btn-info btn-view" type="button">Edit</button>'
	        } ]
	    } );

	    $('#float tbody').on( 'click', 'button', function () {
	        save_method = 'update';
	        $('#form')[0].reset();
	        var data = table.row( $(this).parents('tr') ).data();
	        // alert( data[0] +"'s salary is: "+ data[ 5 ] );
	        var id = data[0];

	            $.ajax({
	            url : "<?php echo site_url('mpesaadmin/edit/')?>/" + data[0],
	            type: "GET",
	            dataType: "JSON",
	            success: function(data)
	            {
	     
	                $('[name="id"]').val(data.id);
	                $('[name="cash"]').val(data.cash);
	                $('[name="float"]').val(data.float);
	                $('[name="shop_name"]').val(data.shop_name);
	     
	     
	                $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
	                $('.modal-title').text('Edit Float'); // Set title to Bootstrap modal title
	     
	            },
	            error: function (jqXHR, textStatus, errorThrown)
	            {
	                alert('Error get data from ajax');
	            }
	        });


	    } );


	} );

	function save(){
        var url;
          if(save_method == 'add')
          {
              url = "<?php echo site_url('mpesa/addTransaction')?>";
          }
          else
          {
            url = "<?php echo site_url('mpesaadmin/updatefloat')?>";
          }

          $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               $('#modal_form').modal('hide');
               alert("updated / Inserted successfully")
              location.reload();// for reload a page
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
        
    }

</script>

<div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Mpesa Transaction Form</h3>
      </div>
      <div class="modal-body form">
        <?php echo form_open('#', array('id'=>'form', 'enctype'=>'multipart/form-data', 'class'=>'form-horizontal')); ?>
            <input type="hidden" value="<?php echo $this->security->get_csrf_hash(); ?>" name="<?php echo $this->security->get_csrf_token_name(); ?>">
          <div class="form-body">
            <input type="hidden" name="id" value="">

            <div class="form-group form-group-sm">
            <?php echo form_label('Shop Name', 'shop_name', array('class'=>'required control-label col-xs-3')); ?>
            <div class='col-xs-8'>
                <?php echo form_input(array(
                        'name'=>'shop_name',
                        'id'=>'shop_name',
                        'type' => 'text',
                        'class'=>'form-control input-sm')
                        );?>
            </div>
        </div>

        <div class="form-group form-group-sm">
            <?php echo form_label('Cash', 'cash', array('class'=>'required control-label col-xs-3')); ?>
            <div class='col-xs-8'>
                <?php echo form_input(array(
                        'name'=>'cash',
                        'id'=>'cash',
                        'type' => 'number',
                        'class'=>'form-control input-sm')
                        );?>
            </div>
        </div>

        <div class="form-group form-group-sm">
            <?php echo form_label('Float', 'l=float', array('class'=>'required control-label col-xs-3')); ?>
            <div class='col-xs-8'>
                <?php echo form_input(array(
                        'name'=>'float',
                        'id'=>'float',
                        'type' => 'number',
                        'class'=>'form-control input-sm')
                        );?>
            </div>
        </div>
 
          </div>
        <?php echo form_close(); ?>
          </div>
          <div class="modal-footer">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->