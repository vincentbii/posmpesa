<?php $this->load->view("partial/header"); ?><div class="row">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
	<div class="row">
  <h1>Credits Management</h1>
	<table id="credits" class="display" style="width:100%">
		<thead>
			<tr>
				<th>#</th>
				<th>Denomination</th>
				<th>Packets in Store</th>
				<th>Buying Price</th>
        <th>Wholesale Price</th>
        <th>Retail Price</th>
				<th>Action</th>
			</tr>
		</thead>
	</table>
</div>
<div class="row">
  <h3>Credit Transactions Detailed Report</h3>
  <table id="allTransactions" class="display">
    <thead>
      <tr>
        <th>#</th>
        <th>Credit</th>
        <th>Amount</th>
        <th>Transaction Date</th>
        <th>System User</th>
        <th>Status</th>
        <th>Action</th>
      </tr>
    </thead>
  </table>
</div>
<div class="row">
  <h3>Credit Transactions Summary Report</h3>
  <table id="summaryTransactions" class="display">
    <thead>
      <tr>
        <th>Credit</th>
        <th>Total Transactions Made</th>
      </tr>
    </thead>
  </table>
</div>
<?php $this->load->view("partial/footer"); ?>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
	    var table = $('#credits').DataTable( {
        "dom": 'Bfrtip',
          "buttons": [
              {
                  "text": 'Issue Items',
                  "className": 'Issue_items',
                  action: function ( e, dt, node, config ) {
                      window.location.href = "<?php echo site_url("credits/issueDetails"); ?>";
                      // alert("HI")
                  }
              }
          ],
	        "ajax": "<?php echo site_url("credits_admin/getCredit") ?>",
	        "columns": [
	            { "data": "id" },
	            { "data": "denomination" },
              { "data": "qty" },
	            { "data": "wprice" },
	            { "data": "rprice" },
	            { "data": "bprice" },
	            { "data": {
	            	"targets": -1,
	            	"data": "id",
	            	"defaultContent": "<button class='btn btn-xs btn-success'><i class='glyphicon glyphicon-edit'></i></button>"
	            }}
	        ],
	        "columnDefs": [ {
	            	"targets": -1,
	            	"data": "id",
	            	"defaultContent": "<button id='add_item' class='btn btn-xs btn-success'><i class='glyphicon glyphicon-plus'></i></button>"
	        } ]
	    } );

      var table1 = $('#allTransactions').DataTable( {
          "ajax": "<?php echo site_url("credits_admin/getalltransactions") ?>",
          "columns": [
              { "data": "id" },
              { "data": "credit" },
              { "data": "amount" },
              { "data": "datetime" },
              { 
                "data": null,
                render: function (data, type, row) {
                        var details = row.last_name + " " + row.first_name;
                        return details;
                    }
                },
              { 
                "data": null,
                render: function (data, type, row) {
                  var stat = '';
                  if(row.status = 1){
                    stat = 'Active';
                  }else{
                    stat = 'Deleted';
                  }
                        return stat;
                    }
                },
              { "data": {
                "targets": -1,
                "data": "id",
                "defaultContent": "<button class='btn btn-xs btn-success'><i class='glyphicon glyphicon-trash'></i></button>"
              }}
          ],
          "columnDefs": [ {
                "targets": -1,
                "data": "id",
                "defaultContent": "<button class='btn btn-xs btn-success'><i class='glyphicon glyphicon-edit'></i></button>"
          } ]
      } );

      var table2 = $('#summaryTransactions').DataTable( {
          "ajax": "<?php echo site_url("credits_admin/getTransactionsSummary") ?>",
          "columns": [
              { "data": "credit" }
          ]
      } );

	    $('#credits tbody').on( 'click', '#add_item', function () {
	    	save_method = 'add';
	    	$('#form')[0].reset();
        	var data = table.row( $(this).parents('tr') ).data();
        	// alert( data['id'] +"'s salary is: "+ data['selling_price'] );
          $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
          $('[name="denomination"]').val(data.denomination);
          $('[name="id"]').val(data.id);
          $('.modal-title').text('Add packets');
    	} );
	} );

	function save(){
        var url;
          if(save_method == 'add')
          {
              url = "<?php echo site_url('credits/addReceivables')?>";
          }
          else
          {
            url = "<?php echo site_url('credits_admin/update')?>";
          }

          $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               $('#modal_form').modal('hide');
               window.location.href = '<?php echo site_url("credits"); ?>/' + data.id + '/' + data.qty;
              // location.reload();// for reload a page
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
        
    }

</script>
<div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Receive Items</h3>
      </div>
      <div class="modal-body form">
        <?php echo form_open('#', array('id'=>'form', 'enctype'=>'multipart/form-data', 'class'=>'form-horizontal')); ?>
            <input type="hidden" value="<?php echo $this->security->get_csrf_hash(); ?>" name="<?php echo $this->security->get_csrf_token_name(); ?>">

            <input type="hidden" name="id">
          <div class="form-body">
        <div class="form-group form-group-sm">
            <?php echo form_label('Denomination', 'denomination', array('class'=>'required control-label col-xs-3')); ?>
            <div class='col-xs-8'>
                <?php echo form_input(array(
                        'name'=>'denomination',
                        'id'=>'denomination',
                        'type' => 'text',
                        'class'=>'form-control input-sm')
                        );?>
            </div>
        </div>

        <div class="form-group form-group-sm">
            <?php echo form_label('Packet Quantity', 'quantity', array('class'=>'required control-label col-xs-3')); ?>
            <div class='col-xs-8'>
                <?php echo form_input(array(
                        'name'=>'quantity',
                        'id'=>'quantity',
                        'type' => 'number',
                        'class'=>'form-control input-sm')
                        );?>
            </div>
        </div>
 
          </div>
        <?php echo form_close(); ?>
          </div>
          <div class="modal-footer">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->