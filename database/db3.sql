-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: opensourcepos
-- ------------------------------------------------------
-- Server version	5.7.19-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ospos_app_config`
--

DROP TABLE IF EXISTS `ospos_app_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_app_config` (
  `key` varchar(50) NOT NULL,
  `value` varchar(500) NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_app_config`
--

LOCK TABLES `ospos_app_config` WRITE;
/*!40000 ALTER TABLE `ospos_app_config` DISABLE KEYS */;
INSERT INTO `ospos_app_config` VALUES ('address','P.O Box XXXX'),('allow_duplicate_barcodes','0'),('barcode_content','id'),('barcode_first_row','category'),('barcode_font','Arial'),('barcode_font_size','10'),('barcode_formats','[]'),('barcode_generate_if_empty','0'),('barcode_height','50'),('barcode_num_in_row','2'),('barcode_page_cellspacing','20'),('barcode_page_width','100'),('barcode_second_row','item_code'),('barcode_third_row','unit_price'),('barcode_type','Code39'),('barcode_width','250'),('cash_decimals','2'),('cash_rounding_code','0'),('client_id','d57e0ab6-4607-4171-885b-50494e03e402'),('company','Eclipse Consultants'),('company_logo','company_logo.png'),('country_codes','us'),('currency_decimals','2'),('currency_symbol','$'),('customer_reward_enable','0'),('customer_sales_tax_support','0'),('dateformat','m/d/Y'),('date_or_time_format',''),('default_origin_tax_code',''),('default_register_mode','sale'),('default_sales_discount','0'),('default_tax_category','Standard'),('derive_sale_quantity','0'),('dinner_table_enable','0'),('email',''),('email_receipt_check_behaviour','last'),('fax',''),('financial_year','1'),('gcaptcha_enable','0'),('gcaptcha_secret_key',''),('gcaptcha_site_key',''),('giftcard_number','series'),('invoice_default_comments','This is a default comment'),('invoice_email_message','Dear {CU}, In attachment the receipt for sale {ISEQ}'),('invoice_enable','1'),('language','english'),('language_code','en-US'),('last_used_invoice_number','0'),('last_used_quote_number','0'),('last_used_work_order_number','0'),('lines_per_page','25'),('line_sequence','0'),('mailpath','/usr/sbin/sendmail'),('msg_msg',''),('msg_pwd',''),('msg_src',''),('msg_uid',''),('notify_horizontal_position','center'),('notify_vertical_position','bottom'),('number_locale','en_US'),('payment_options_order','cashdebitcredit'),('phone','0770369799'),('print_bottom_margin','0'),('print_footer','0'),('print_header','0'),('print_left_margin','0'),('print_receipt_check_behaviour','last'),('print_right_margin','0'),('print_silently','1'),('print_top_margin','0'),('protocol','mail'),('quantity_decimals','0'),('quote_default_comments','This is a default quote comment'),('receipt_font_size','12'),('receipt_show_company_name','1'),('receipt_show_description','1'),('receipt_show_serialnumber','1'),('receipt_show_taxes','0'),('receipt_show_total_discount','1'),('receipt_template','receipt_default'),('receiving_calculate_average_price','0'),('recv_invoice_format','{CO}'),('return_policy','Return Policy'),('sales_invoice_format','{CO}'),('sales_quote_format','Q%y{QSEQ:6}'),('smtp_crypto','ssl'),('smtp_host',''),('smtp_pass',''),('smtp_port','465'),('smtp_timeout','5'),('smtp_user',''),('suggestions_first_column','name'),('suggestions_second_column',''),('suggestions_third_column',''),('tax_decimals','2'),('tax_included','0'),('theme','flatly'),('thousands_separator','1'),('timeformat','H:i:s'),('timezone','America/New_York'),('website',''),('work_order_enable','0'),('work_order_format','W%y{WSEQ:6}');
/*!40000 ALTER TABLE `ospos_app_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_credit_denominations`
--

DROP TABLE IF EXISTS `ospos_credit_denominations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_credit_denominations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `denomination` varchar(45) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_credit_denominations`
--

LOCK TABLES `ospos_credit_denominations` WRITE;
/*!40000 ALTER TABLE `ospos_credit_denominations` DISABLE KEYS */;
INSERT INTO `ospos_credit_denominations` VALUES (1,'10',1,'2018-06-14 20:18:19','2018-06-14 20:18:52'),(2,'20',1,'2018-06-14 20:18:20','2018-06-14 20:18:52'),(3,'50',1,'2018-06-14 20:18:20','2018-06-14 20:18:52'),(4,'100',1,'2018-06-14 20:18:20','2018-06-14 20:18:52'),(5,'250',1,'2018-06-14 20:18:20','2018-06-14 20:18:52'),(6,'500',1,'2018-06-14 20:18:20','2018-06-14 20:18:52'),(7,'1000',1,'2018-06-14 20:18:20','2018-06-14 20:18:52');
/*!40000 ALTER TABLE `ospos_credit_denominations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_credit_transactions`
--

DROP TABLE IF EXISTS `ospos_credit_transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_credit_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) DEFAULT NULL,
  `credit` int(11) DEFAULT NULL,
  `amount` float DEFAULT '0',
  `discount` varchar(45) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `User ID_idx` (`user_id`),
  KEY `Credit_idx` (`credit`),
  CONSTRAINT `Credit` FOREIGN KEY (`credit`) REFERENCES `ospos_credits` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `User ID` FOREIGN KEY (`user_id`) REFERENCES `ospos_employees` (`person_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_credit_transactions`
--

LOCK TABLES `ospos_credit_transactions` WRITE;
/*!40000 ALTER TABLE `ospos_credit_transactions` DISABLE KEYS */;
INSERT INTO `ospos_credit_transactions` VALUES (1,'2018-06-16 02:52:46',1,2,41,NULL,1,'2018-06-16 02:52:46','2018-06-16 04:07:29'),(2,'2018-06-16 03:07:27',2,5,48,NULL,1,'2018-06-16 03:07:27','2018-06-16 05:01:34'),(3,'2018-06-16 04:02:33',2,4,18,NULL,1,'2018-06-16 04:02:33','2018-06-16 05:01:46'),(4,'2018-06-16 04:50:59',2,1,85,NULL,1,'2018-06-16 04:50:59','2018-06-16 04:50:59'),(5,'2018-06-16 12:58:56',2,1,1,NULL,1,'2018-06-16 12:58:56','2018-06-16 12:58:56'),(6,'2018-06-16 12:59:04',2,1,1,NULL,1,'2018-06-16 12:59:04','2018-06-16 12:59:04'),(7,'2018-06-16 12:59:59',2,1,1,NULL,1,'2018-06-16 12:59:59','2018-06-16 12:59:59'),(8,'2018-06-16 13:00:13',2,1,8,NULL,1,'2018-06-16 13:00:13','2018-06-16 13:00:13');
/*!40000 ALTER TABLE `ospos_credit_transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_credit_type`
--

DROP TABLE IF EXISTS `ospos_credit_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_credit_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `credit_type` varchar(45) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_credit_type`
--

LOCK TABLES `ospos_credit_type` WRITE;
/*!40000 ALTER TABLE `ospos_credit_type` DISABLE KEYS */;
INSERT INTO `ospos_credit_type` VALUES (1,'Retail',1,'2018-06-14 20:19:46','2018-06-14 20:19:46'),(2,'Wholesale',1,'2018-06-14 20:19:46','2018-06-14 20:19:46');
/*!40000 ALTER TABLE `ospos_credit_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_credits`
--

DROP TABLE IF EXISTS `ospos_credits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_credits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `credit` varchar(45) DEFAULT NULL,
  `denomination` int(11) DEFAULT NULL,
  `credit_type` int(11) DEFAULT NULL,
  `buying_price` float DEFAULT '0',
  `selling_price` float DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(4) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `updated_by_idx` (`updated_by`),
  KEY `created_by_idx` (`created_by`),
  KEY `denomination_idx` (`denomination`),
  KEY `credit_type_idx` (`credit_type`),
  CONSTRAINT `created_by` FOREIGN KEY (`created_by`) REFERENCES `ospos_people` (`person_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `credit_type` FOREIGN KEY (`credit_type`) REFERENCES `ospos_credit_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `denomination` FOREIGN KEY (`denomination`) REFERENCES `ospos_credit_denominations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `updated_by` FOREIGN KEY (`updated_by`) REFERENCES `ospos_people` (`person_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_credits`
--

LOCK TABLES `ospos_credits` WRITE;
/*!40000 ALTER TABLE `ospos_credits` DISABLE KEYS */;
INSERT INTO `ospos_credits` VALUES (1,'10 (Retail)',1,1,74,74,'2018-06-14 20:21:16','2018-06-15 15:00:08',1,NULL,NULL),(2,'10 (Wholesale)',1,2,7,10,'2018-06-14 20:21:16','2018-06-16 14:18:53',1,NULL,NULL),(3,'20 (Retail)',2,1,0,0,'2018-06-14 20:21:16','2018-06-15 00:06:09',1,NULL,NULL),(4,'20 (Wholesale)',2,2,0,0,'2018-06-14 20:21:16','2018-06-15 00:06:09',1,NULL,NULL),(5,'50 (Retail)',3,1,0,0,'2018-06-14 20:21:16','2018-06-15 00:06:09',1,NULL,NULL),(6,'50 (Wholesale)',3,2,0,0,'2018-06-14 20:21:16','2018-06-15 00:06:09',1,NULL,NULL),(7,'100 (Retail)',4,1,0,0,'2018-06-14 20:21:16','2018-06-15 00:06:09',1,NULL,NULL),(8,'100 (Wholesale)',4,2,0,0,'2018-06-14 20:21:16','2018-06-15 00:06:09',1,NULL,NULL),(9,'250 (Retail)',5,1,0,0,'2018-06-14 20:21:16','2018-06-15 00:06:09',1,NULL,NULL),(10,'250 (Wholesale)',5,2,0,0,'2018-06-14 20:21:16','2018-06-15 00:06:09',1,NULL,NULL),(11,'500 (Retail)',6,1,0,0,'2018-06-14 20:21:16','2018-06-15 00:06:09',1,NULL,NULL),(12,'500 (Wholesale)',6,2,0,0,'2018-06-14 20:21:16','2018-06-15 00:06:09',1,NULL,NULL),(13,'1000 (Retail)',7,1,0,0,'2018-06-14 20:21:16','2018-06-15 00:06:09',1,NULL,NULL),(14,'1000 (Wholesale)',7,2,0,0,'2018-06-14 20:21:16','2018-06-15 00:06:09',1,NULL,NULL);
/*!40000 ALTER TABLE `ospos_credits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_credits_received_packets`
--

DROP TABLE IF EXISTS `ospos_credits_received_packets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_credits_received_packets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `denomination` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `qty_packets` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_idx` (`user`),
  KEY `denomination_idx` (`denomination`),
  CONSTRAINT `credit denomination` FOREIGN KEY (`denomination`) REFERENCES `ospos_credit_denominations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user` FOREIGN KEY (`user`) REFERENCES `ospos_people` (`person_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_credits_received_packets`
--

LOCK TABLES `ospos_credits_received_packets` WRITE;
/*!40000 ALTER TABLE `ospos_credits_received_packets` DISABLE KEYS */;
INSERT INTO `ospos_credits_received_packets` VALUES (1,1,1,'5','2018-06-18 19:32:41','2018-06-18 19:32:41',2),(2,1,1,'2','2018-06-18 19:34:16','2018-06-18 19:34:16',2);
/*!40000 ALTER TABLE `ospos_credits_received_packets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_customers`
--

DROP TABLE IF EXISTS `ospos_customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_customers` (
  `person_id` int(10) NOT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `account_number` varchar(255) DEFAULT NULL,
  `taxable` int(1) NOT NULL DEFAULT '1',
  `sales_tax_code` varchar(32) NOT NULL DEFAULT '1',
  `discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `package_id` int(11) DEFAULT NULL,
  `points` int(11) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `account_number` (`account_number`),
  KEY `person_id` (`person_id`),
  KEY `package_id` (`package_id`),
  CONSTRAINT `ospos_customers_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `ospos_people` (`person_id`),
  CONSTRAINT `ospos_customers_ibfk_2` FOREIGN KEY (`package_id`) REFERENCES `ospos_customers_packages` (`package_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_customers`
--

LOCK TABLES `ospos_customers` WRITE;
/*!40000 ALTER TABLE `ospos_customers` DISABLE KEYS */;
INSERT INTO `ospos_customers` VALUES (3,NULL,NULL,1,'',0.00,NULL,NULL,0);
/*!40000 ALTER TABLE `ospos_customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_customers_packages`
--

DROP TABLE IF EXISTS `ospos_customers_packages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_customers_packages` (
  `package_id` int(11) NOT NULL AUTO_INCREMENT,
  `package_name` varchar(255) DEFAULT NULL,
  `points_percent` float NOT NULL DEFAULT '0',
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`package_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_customers_packages`
--

LOCK TABLES `ospos_customers_packages` WRITE;
/*!40000 ALTER TABLE `ospos_customers_packages` DISABLE KEYS */;
INSERT INTO `ospos_customers_packages` VALUES (1,'Default',0,0),(2,'Bronze',10,0),(3,'Silver',20,0),(4,'Gold',30,0),(5,'Premium',50,0),(6,'Default',0,0),(7,'Bronze',10,0),(8,'Silver',20,0),(9,'Gold',30,0),(10,'Premium',50,0),(11,'Default',0,0),(12,'Bronze',10,0),(13,'Silver',20,0),(14,'Gold',30,0),(15,'Premium',50,0);
/*!40000 ALTER TABLE `ospos_customers_packages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_customers_points`
--

DROP TABLE IF EXISTS `ospos_customers_points`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_customers_points` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `sale_id` int(11) NOT NULL,
  `points_earned` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`),
  KEY `package_id` (`package_id`),
  KEY `sale_id` (`sale_id`),
  CONSTRAINT `ospos_customers_points_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `ospos_customers` (`person_id`),
  CONSTRAINT `ospos_customers_points_ibfk_2` FOREIGN KEY (`package_id`) REFERENCES `ospos_customers_packages` (`package_id`),
  CONSTRAINT `ospos_customers_points_ibfk_3` FOREIGN KEY (`sale_id`) REFERENCES `ospos_sales` (`sale_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_customers_points`
--

LOCK TABLES `ospos_customers_points` WRITE;
/*!40000 ALTER TABLE `ospos_customers_points` DISABLE KEYS */;
/*!40000 ALTER TABLE `ospos_customers_points` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_dinner_tables`
--

DROP TABLE IF EXISTS `ospos_dinner_tables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_dinner_tables` (
  `dinner_table_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`dinner_table_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_dinner_tables`
--

LOCK TABLES `ospos_dinner_tables` WRITE;
/*!40000 ALTER TABLE `ospos_dinner_tables` DISABLE KEYS */;
INSERT INTO `ospos_dinner_tables` VALUES (1,'Delivery',0,0),(2,'Take Away',0,0),(3,'Delivery',0,0),(4,'Take Away',0,0),(5,'Delivery',0,0),(6,'Take Away',0,0);
/*!40000 ALTER TABLE `ospos_dinner_tables` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_employees`
--

DROP TABLE IF EXISTS `ospos_employees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_employees` (
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `person_id` int(10) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `hash_version` int(1) NOT NULL DEFAULT '2',
  `language` varchar(48) DEFAULT NULL,
  `language_code` varchar(8) DEFAULT NULL,
  UNIQUE KEY `username` (`username`),
  KEY `person_id` (`person_id`),
  CONSTRAINT `ospos_employees_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `ospos_people` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_employees`
--

LOCK TABLES `ospos_employees` WRITE;
/*!40000 ALTER TABLE `ospos_employees` DISABLE KEYS */;
INSERT INTO `ospos_employees` VALUES ('admin','*B37ACB9927C1F3B520BBF976386EAB76A08F3367',1,0,2,NULL,NULL),('vincent','$2y$10$3w7D1U7FyMDwlDMaK.7DaO2Gj8AF7.Q3cSnlclE6sWnPqYCGzaaCm',2,0,2,NULL,NULL);
/*!40000 ALTER TABLE `ospos_employees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_expense_categories`
--

DROP TABLE IF EXISTS `ospos_expense_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_expense_categories` (
  `expense_category_id` int(10) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) DEFAULT NULL,
  `category_description` varchar(255) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`expense_category_id`),
  UNIQUE KEY `category_name` (`category_name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_expense_categories`
--

LOCK TABLES `ospos_expense_categories` WRITE;
/*!40000 ALTER TABLE `ospos_expense_categories` DISABLE KEYS */;
INSERT INTO `ospos_expense_categories` VALUES (1,'Utilities','Water',0);
/*!40000 ALTER TABLE `ospos_expense_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_expenses`
--

DROP TABLE IF EXISTS `ospos_expenses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_expenses` (
  `expense_id` int(10) NOT NULL AUTO_INCREMENT,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `amount` decimal(15,2) NOT NULL,
  `payment_type` varchar(40) NOT NULL,
  `expense_category_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `employee_id` int(10) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `supplier_name` varchar(255) DEFAULT NULL,
  `supplier_tax_code` varchar(255) DEFAULT NULL,
  `tax_amount` decimal(15,2) DEFAULT NULL,
  PRIMARY KEY (`expense_id`),
  KEY `expense_category_id` (`expense_category_id`),
  KEY `employee_id` (`employee_id`),
  CONSTRAINT `ospos_expenses_ibfk_1` FOREIGN KEY (`expense_category_id`) REFERENCES `ospos_expense_categories` (`expense_category_id`),
  CONSTRAINT `ospos_expenses_ibfk_2` FOREIGN KEY (`employee_id`) REFERENCES `ospos_employees` (`person_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_expenses`
--

LOCK TABLES `ospos_expenses` WRITE;
/*!40000 ALTER TABLE `ospos_expenses` DISABLE KEYS */;
INSERT INTO `ospos_expenses` VALUES (1,'2017-04-20 04:00:00',15.00,'',1,'Water',1,0,NULL,NULL,NULL);
/*!40000 ALTER TABLE `ospos_expenses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_giftcards`
--

DROP TABLE IF EXISTS `ospos_giftcards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_giftcards` (
  `record_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `giftcard_id` int(11) NOT NULL AUTO_INCREMENT,
  `giftcard_number` varchar(255) DEFAULT NULL,
  `value` decimal(15,2) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `person_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`giftcard_id`),
  UNIQUE KEY `giftcard_number` (`giftcard_number`),
  KEY `person_id` (`person_id`),
  CONSTRAINT `ospos_giftcards_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `ospos_people` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_giftcards`
--

LOCK TABLES `ospos_giftcards` WRITE;
/*!40000 ALTER TABLE `ospos_giftcards` DISABLE KEYS */;
/*!40000 ALTER TABLE `ospos_giftcards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_grants`
--

DROP TABLE IF EXISTS `ospos_grants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_grants` (
  `permission_id` varchar(255) NOT NULL,
  `person_id` int(10) NOT NULL,
  `menu_group` varchar(32) DEFAULT 'home',
  PRIMARY KEY (`permission_id`,`person_id`),
  KEY `ospos_grants_ibfk_2` (`person_id`),
  CONSTRAINT `ospos_grants_ibfk_1` FOREIGN KEY (`permission_id`) REFERENCES `ospos_permissions` (`permission_id`) ON DELETE CASCADE,
  CONSTRAINT `ospos_grants_ibfk_2` FOREIGN KEY (`person_id`) REFERENCES `ospos_employees` (`person_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_grants`
--

LOCK TABLES `ospos_grants` WRITE;
/*!40000 ALTER TABLE `ospos_grants` DISABLE KEYS */;
INSERT INTO `ospos_grants` VALUES ('config',1,'office'),('credits',2,'home'),('credits_admin',2,'home'),('customers',1,'home'),('customers',2,'home'),('employees',1,'office'),('expenses',1,'home'),('expenses_categories',1,'home'),('giftcards',1,'home'),('home',1,'office'),('home',2,'home'),('items',1,'home'),('items',2,'home'),('items_stock',1,'home'),('items_stock',2,'home'),('item_kits',1,'home'),('item_kits',2,'home'),('messages',1,'home'),('mpesa',2,'home'),('mpesaadmin',2,'home'),('office',1,'home'),('receivings',1,'home'),('receivings',2,'home'),('receivings_stock',1,'home'),('receivings_stock',2,'home'),('reports',1,'home'),('reports',2,'home'),('reports_categories',1,'home'),('reports_categories',2,'home'),('reports_customers',1,'home'),('reports_discounts',1,'home'),('reports_employees',1,'home'),('reports_employees',2,'home'),('reports_expenses_categories',1,'home'),('reports_expenses_categories',2,'home'),('reports_inventory',1,'home'),('reports_inventory',2,'home'),('reports_items',1,'home'),('reports_items',2,'home'),('reports_payments',1,'home'),('reports_payments',2,'home'),('reports_receivings',1,'home'),('reports_receivings',2,'home'),('reports_sales',1,'home'),('reports_sales',2,'home'),('reports_suppliers',1,'home'),('reports_suppliers',2,'home'),('reports_taxes',1,'home'),('reports_taxes',2,'home'),('sales',1,'home'),('sales',2,'home'),('sales_delete',1,'--'),('sales_delete',2,'home'),('sales_stock',1,'home'),('sales_stock',2,'home'),('suppliers',1,'home'),('taxes',1,'office');
/*!40000 ALTER TABLE `ospos_grants` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_inventory`
--

DROP TABLE IF EXISTS `ospos_inventory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_inventory` (
  `trans_id` int(11) NOT NULL AUTO_INCREMENT,
  `trans_items` int(11) NOT NULL DEFAULT '0',
  `trans_user` int(11) NOT NULL DEFAULT '0',
  `trans_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `trans_comment` text NOT NULL,
  `trans_location` int(11) NOT NULL,
  `trans_inventory` decimal(15,3) NOT NULL DEFAULT '0.000',
  PRIMARY KEY (`trans_id`),
  KEY `trans_items` (`trans_items`),
  KEY `trans_user` (`trans_user`),
  KEY `trans_location` (`trans_location`),
  CONSTRAINT `ospos_inventory_ibfk_1` FOREIGN KEY (`trans_items`) REFERENCES `ospos_items` (`item_id`),
  CONSTRAINT `ospos_inventory_ibfk_2` FOREIGN KEY (`trans_user`) REFERENCES `ospos_employees` (`person_id`),
  CONSTRAINT `ospos_inventory_ibfk_3` FOREIGN KEY (`trans_location`) REFERENCES `ospos_stock_locations` (`location_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_inventory`
--

LOCK TABLES `ospos_inventory` WRITE;
/*!40000 ALTER TABLE `ospos_inventory` DISABLE KEYS */;
INSERT INTO `ospos_inventory` VALUES (1,1,2,'2018-04-26 04:17:05','Manual Edit of Quantity',1,1000.000),(2,2,2,'2018-04-26 04:18:20','Manual Edit of Quantity',1,100.000),(3,2,2,'2018-04-26 04:18:49','RECV 1',1,1.000),(4,2,2,'2018-04-26 04:20:37','POS 1',1,-10.000);
/*!40000 ALTER TABLE `ospos_inventory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_item_kit_items`
--

DROP TABLE IF EXISTS `ospos_item_kit_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_item_kit_items` (
  `item_kit_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `quantity` decimal(15,3) NOT NULL,
  `kit_sequence` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`item_kit_id`,`item_id`,`quantity`),
  KEY `ospos_item_kit_items_ibfk_2` (`item_id`),
  CONSTRAINT `ospos_item_kit_items_ibfk_1` FOREIGN KEY (`item_kit_id`) REFERENCES `ospos_item_kits` (`item_kit_id`) ON DELETE CASCADE,
  CONSTRAINT `ospos_item_kit_items_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `ospos_items` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_item_kit_items`
--

LOCK TABLES `ospos_item_kit_items` WRITE;
/*!40000 ALTER TABLE `ospos_item_kit_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `ospos_item_kit_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_item_kits`
--

DROP TABLE IF EXISTS `ospos_item_kits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_item_kits` (
  `item_kit_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `item_id` int(10) NOT NULL DEFAULT '0',
  `kit_discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `price_option` tinyint(2) NOT NULL DEFAULT '0',
  `print_option` tinyint(2) NOT NULL DEFAULT '0',
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`item_kit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_item_kits`
--

LOCK TABLES `ospos_item_kits` WRITE;
/*!40000 ALTER TABLE `ospos_item_kits` DISABLE KEYS */;
INSERT INTO `ospos_item_kits` VALUES (1,'Kit One',0,0.00,0,0,'');
/*!40000 ALTER TABLE `ospos_item_kits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_item_quantities`
--

DROP TABLE IF EXISTS `ospos_item_quantities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_item_quantities` (
  `item_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `quantity` decimal(15,3) NOT NULL DEFAULT '0.000',
  PRIMARY KEY (`item_id`,`location_id`),
  KEY `item_id` (`item_id`),
  KEY `location_id` (`location_id`),
  CONSTRAINT `ospos_item_quantities_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `ospos_items` (`item_id`),
  CONSTRAINT `ospos_item_quantities_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `ospos_stock_locations` (`location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_item_quantities`
--

LOCK TABLES `ospos_item_quantities` WRITE;
/*!40000 ALTER TABLE `ospos_item_quantities` DISABLE KEYS */;
INSERT INTO `ospos_item_quantities` VALUES (1,1,1000.000),(2,1,91.000);
/*!40000 ALTER TABLE `ospos_item_quantities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_items`
--

DROP TABLE IF EXISTS `ospos_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_items` (
  `name` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `item_number` varchar(255) DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  `cost_price` decimal(15,2) NOT NULL,
  `unit_price` decimal(15,2) NOT NULL,
  `reorder_level` decimal(15,3) NOT NULL DEFAULT '0.000',
  `receiving_quantity` decimal(15,3) NOT NULL DEFAULT '1.000',
  `item_id` int(10) NOT NULL AUTO_INCREMENT,
  `pic_filename` varchar(255) DEFAULT NULL,
  `allow_alt_description` tinyint(1) NOT NULL,
  `is_serialized` tinyint(1) NOT NULL,
  `stock_type` tinyint(2) NOT NULL DEFAULT '0',
  `item_type` tinyint(2) NOT NULL DEFAULT '0',
  `tax_category_id` int(10) NOT NULL DEFAULT '1',
  `deleted` int(1) NOT NULL DEFAULT '0',
  `custom1` varchar(255) DEFAULT NULL,
  `custom2` varchar(255) DEFAULT NULL,
  `custom3` varchar(255) DEFAULT NULL,
  `custom4` varchar(255) DEFAULT NULL,
  `custom5` varchar(255) DEFAULT NULL,
  `custom6` varchar(255) DEFAULT NULL,
  `custom7` varchar(255) DEFAULT NULL,
  `custom8` varchar(255) DEFAULT NULL,
  `custom9` varchar(255) DEFAULT NULL,
  `custom10` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  KEY `item_number` (`item_number`),
  KEY `supplier_id` (`supplier_id`),
  CONSTRAINT `ospos_items_ibfk_1` FOREIGN KEY (`supplier_id`) REFERENCES `ospos_suppliers` (`person_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_items`
--

LOCK TABLES `ospos_items` WRITE;
/*!40000 ALTER TABLE `ospos_items` DISABLE KEYS */;
INSERT INTO `ospos_items` VALUES ('Phone','1',NULL,NULL,'',14000.00,20000.00,100.000,1000.000,1,NULL,0,1,0,0,0,0,'','','','','','','','','',''),('Samsang Calaxy','phones',NULL,NULL,'',15000.00,25000.00,1.000,1.000,2,NULL,0,0,0,0,0,0,'','','','','','','','','','');
/*!40000 ALTER TABLE `ospos_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_items_taxes`
--

DROP TABLE IF EXISTS `ospos_items_taxes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_items_taxes` (
  `item_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `percent` decimal(15,3) NOT NULL,
  PRIMARY KEY (`item_id`,`name`,`percent`),
  CONSTRAINT `ospos_items_taxes_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `ospos_items` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_items_taxes`
--

LOCK TABLES `ospos_items_taxes` WRITE;
/*!40000 ALTER TABLE `ospos_items_taxes` DISABLE KEYS */;
/*!40000 ALTER TABLE `ospos_items_taxes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_migrations`
--

DROP TABLE IF EXISTS `ospos_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_migrations`
--

LOCK TABLES `ospos_migrations` WRITE;
/*!40000 ALTER TABLE `ospos_migrations` DISABLE KEYS */;
INSERT INTO `ospos_migrations` VALUES (20180225100000);
/*!40000 ALTER TABLE `ospos_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_modules`
--

DROP TABLE IF EXISTS `ospos_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_modules` (
  `name_lang_key` varchar(255) NOT NULL,
  `desc_lang_key` varchar(255) NOT NULL,
  `sort` int(10) NOT NULL,
  `module_id` varchar(255) NOT NULL,
  PRIMARY KEY (`module_id`),
  UNIQUE KEY `desc_lang_key` (`desc_lang_key`),
  UNIQUE KEY `name_lang_key` (`name_lang_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_modules`
--

LOCK TABLES `ospos_modules` WRITE;
/*!40000 ALTER TABLE `ospos_modules` DISABLE KEYS */;
INSERT INTO `ospos_modules` VALUES ('module_config','module_config_desc',110,'config'),('module_credits','module_credits_desc',106,'credits'),('module_credits_admin','module_credits_admin_desc',107,'credits_admin'),('module_customers','module_customers_desc',10,'customers'),('module_employees','module_employees_desc',80,'employees'),('module_expenses','module_expenses_desc',108,'expenses'),('module_expenses_categories','module_expenses_categories_desc',109,'expenses_categories'),('module_giftcards','module_giftcards_desc',90,'giftcards'),('module_home','module_home_desc',1,'home'),('module_items','module_items_desc',20,'items'),('module_item_kits','module_item_kits_desc',30,'item_kits'),('module_messages','module_messages_desc',98,'messages'),('module_mpesa','module_mpesa_desc',100,'mpesa'),('module_mpesaadmin','module_mpesa_admin',101,'mpesaadmin'),('module_office','module_office_desc',999,'office'),('module_receivings','module_receivings_desc',60,'receivings'),('module_reports','module_reports_desc',50,'reports'),('module_sales','module_sales_desc',70,'sales'),('module_suppliers','module_suppliers_desc',40,'suppliers'),('module_taxes','module_taxes_desc',105,'taxes');
/*!40000 ALTER TABLE `ospos_modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_mpesa_float`
--

DROP TABLE IF EXISTS `ospos_mpesa_float`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_mpesa_float` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop` int(11) DEFAULT NULL,
  `cash` float DEFAULT '0',
  `float` float DEFAULT '0',
  `date` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `shop_idx` (`shop`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_mpesa_float`
--

LOCK TABLES `ospos_mpesa_float` WRITE;
/*!40000 ALTER TABLE `ospos_mpesa_float` DISABLE KEYS */;
INSERT INTO `ospos_mpesa_float` VALUES (3,3,12000,14000,'2018-05-19 00:00:00'),(4,4,3600,1400,'2018-05-19 00:00:00'),(45,1,0,0,'2018-05-19 00:00:00'),(46,2,26000,-26000,'2018-05-19 00:00:00');
/*!40000 ALTER TABLE `ospos_mpesa_float` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_mpesa_shop`
--

DROP TABLE IF EXISTS `ospos_mpesa_shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_mpesa_shop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_name` varchar(45) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `shop_code` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_mpesa_shop`
--

LOCK TABLES `ospos_mpesa_shop` WRITE;
/*!40000 ALTER TABLE `ospos_mpesa_shop` DISABLE KEYS */;
INSERT INTO `ospos_mpesa_shop` VALUES (1,'Shop 1',1,'2018-05-04 20:17:18','2018-05-12 12:11:28','123456'),(2,'Shop 2',1,'2018-05-04 20:17:18','2018-05-12 12:11:28','85274'),(3,'Shop 3',1,'2018-05-12 12:08:31','2018-05-12 12:11:28','96385'),(4,'Shop 4',1,'2018-05-12 12:08:31','2018-05-12 12:11:28','78912');
/*!40000 ALTER TABLE `ospos_mpesa_shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_mpesa_shop_user`
--

DROP TABLE IF EXISTS `ospos_mpesa_shop_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_mpesa_shop_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop` varchar(100) DEFAULT NULL,
  `user` varchar(20) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_mpesa_shop_user`
--

LOCK TABLES `ospos_mpesa_shop_user` WRITE;
/*!40000 ALTER TABLE `ospos_mpesa_shop_user` DISABLE KEYS */;
INSERT INTO `ospos_mpesa_shop_user` VALUES (89,'\"1\"','admin',NULL,'2018-05-19 10:51:35'),(90,'\"2\"','vincent',NULL,'2018-05-19 10:51:35');
/*!40000 ALTER TABLE `ospos_mpesa_shop_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_mpesa_transaction_type`
--

DROP TABLE IF EXISTS `ospos_mpesa_transaction_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_mpesa_transaction_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(45) DEFAULT NULL,
  `abbr` varchar(10) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='			';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_mpesa_transaction_type`
--

LOCK TABLES `ospos_mpesa_transaction_type` WRITE;
/*!40000 ALTER TABLE `ospos_mpesa_transaction_type` DISABLE KEYS */;
INSERT INTO `ospos_mpesa_transaction_type` VALUES (1,'Deposit','D',1,'2018-05-04 20:13:23','2018-05-04 20:14:38'),(2,'Withdrawal','W',1,'2018-05-04 20:13:23','2018-05-04 20:14:38');
/*!40000 ALTER TABLE `ospos_mpesa_transaction_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_mpesa_transactions`
--

DROP TABLE IF EXISTS `ospos_mpesa_transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_mpesa_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transac_type` int(11) DEFAULT NULL,
  `transact_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `cust_id` int(11) DEFAULT NULL,
  `phone_number` varchar(15) DEFAULT NULL,
  `shop` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `transaction_type_idx` (`transac_type`),
  KEY `user_id_idx` (`user_id`),
  KEY `shop_idx` (`shop`),
  CONSTRAINT `shop` FOREIGN KEY (`shop`) REFERENCES `ospos_mpesa_shop` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `transaction_type` FOREIGN KEY (`transac_type`) REFERENCES `ospos_mpesa_transaction_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `ospos_employees` (`person_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_mpesa_transactions`
--

LOCK TABLES `ospos_mpesa_transactions` WRITE;
/*!40000 ALTER TABLE `ospos_mpesa_transactions` DISABLE KEYS */;
INSERT INTO `ospos_mpesa_transactions` VALUES (15,1,'2018-05-19 15:21:56',2,14700,362541,'07154896',3),(29,1,'2018-05-19 15:21:56',2,1200,362541,'07154896',1),(30,2,'2018-05-19 15:21:56',2,1200,362541,'07154896',2),(31,1,'2018-05-19 15:21:56',2,1400,362541,'07154896',3),(32,2,'2018-05-19 15:21:56',2,15000,362541,'07154896',4),(33,1,'2018-05-19 15:21:56',2,16000,362541,'07154896',1),(34,2,'2018-05-19 15:21:56',2,17000,362541,'07154896',2),(35,1,'2018-05-19 15:21:56',2,100,362541,'07154896',3),(36,2,'2018-05-19 15:21:56',2,60,362541,'07154896',4),(37,1,'2018-05-19 15:21:56',2,180,362541,'07154896',1),(38,2,'2018-05-19 15:21:56',2,230,362541,'07154896',2),(39,1,'2018-05-19 15:21:56',2,250,362541,'07154896',3),(40,1,'2018-05-19 15:21:56',2,780,362541,'07154896',4),(41,1,'2018-05-19 15:21:56',2,45000,20147856,'0711195921',1),(42,2,'2018-05-19 15:22:20',2,14700,7859663,'07158269',1),(43,2,'2018-05-19 18:42:08',2,147000,1452,'1478596',3),(44,1,'2018-05-19 19:00:17',2,70800,254631,'145203',1),(45,1,'2018-05-19 19:02:36',2,14000,741,'85296',3),(46,1,'2018-05-19 19:04:00',2,1234689,1234567,'12345678',1),(47,1,'2018-05-19 19:06:00',2,12000,254123,'254178',2),(48,1,'2018-05-19 19:36:19',2,45000,213,'3245',2),(49,1,'2018-05-19 19:38:18',2,0,0,'',1),(50,1,'2018-05-19 19:42:31',2,2000,25478963,'014785236',2),(51,1,'2018-05-19 19:45:43',2,1200,1456987,'25896314',2),(52,1,'2018-05-19 19:47:14',2,2500,741852,'741859',2),(53,1,'2018-05-19 19:48:44',2,1200,1462179,'52479631',2),(54,1,'2018-05-19 19:49:41',2,14500,5411122,'85963214',2),(55,1,'2018-05-19 19:53:23',2,0,0,'',1),(56,1,'2018-05-19 19:53:32',2,0,0,'',1),(57,1,'2018-05-19 19:54:18',2,0,0,'',1),(58,1,'2018-05-19 19:54:28',2,2345,23456,'123456',1),(59,1,'2018-05-19 19:54:43',2,0,0,'',1),(60,1,'2018-05-19 19:55:00',2,0,0,'',1),(61,1,'2018-05-19 19:55:23',2,0,0,'',1),(62,1,'2018-05-19 19:55:58',2,45000,963741852,'25413',2),(63,1,'2018-05-19 19:57:48',2,1542,147852,'478569',2),(64,2,'2018-05-19 19:58:35',2,2500,47852,'6000',1),(65,1,'2018-05-19 19:58:51',2,0,0,'',1),(66,1,'2018-05-19 19:59:07',2,25,14,'777',3),(67,1,'2018-05-20 06:48:18',2,1200,2541,'258963',2),(68,1,'2018-05-20 06:51:37',2,0,0,'',1),(69,1,'2018-05-20 06:52:36',2,23456,2345,'23456',2),(70,1,'2018-05-20 06:52:41',2,23456,2345,'23456',2),(71,1,'2018-05-20 06:52:58',2,2345678,3456789,'1234567',1),(72,1,'2018-05-20 06:53:16',2,1000,98765434,'7410852',1),(73,1,'2018-05-20 06:54:08',2,1400,234567,'234567',1),(74,1,'2018-05-20 06:54:29',2,0,0,'',1),(75,1,'2018-05-20 06:55:01',2,0,0,'',1),(76,1,'2018-05-20 06:55:45',2,0,0,'',1),(77,1,'2018-05-20 06:59:08',2,0,0,'',1),(78,1,'2018-05-20 06:59:39',2,1200,30229164,'0711195921',2),(79,2,'2018-05-20 07:00:23',2,1200,1582639,'0711195921',2),(80,1,'2018-05-20 07:01:39',2,26000,2541,'14523',2);
/*!40000 ALTER TABLE `ospos_mpesa_transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_people`
--

DROP TABLE IF EXISTS `ospos_people`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_people` (
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `gender` int(1) DEFAULT NULL,
  `phone_number` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address_1` varchar(255) NOT NULL,
  `address_2` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zip` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `comments` text NOT NULL,
  `person_id` int(10) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`person_id`),
  KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_people`
--

LOCK TABLES `ospos_people` WRITE;
/*!40000 ALTER TABLE `ospos_people` DISABLE KEYS */;
INSERT INTO `ospos_people` VALUES ('John','Doe',NULL,'555-555-5555','changeme@example.com','Address 1','','','','','','',1),('Vincent','Bii',1,'','','','','','','','','',2),('Vincent','Bii',0,'0711195921','info@vincentbii.co.ke','','','','','','','',3);
/*!40000 ALTER TABLE `ospos_people` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_permissions`
--

DROP TABLE IF EXISTS `ospos_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_permissions` (
  `permission_id` varchar(255) NOT NULL,
  `module_id` varchar(255) NOT NULL,
  `location_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`permission_id`),
  KEY `module_id` (`module_id`),
  KEY `ospos_permissions_ibfk_2` (`location_id`),
  CONSTRAINT `ospos_permissions_ibfk_1` FOREIGN KEY (`module_id`) REFERENCES `ospos_modules` (`module_id`) ON DELETE CASCADE,
  CONSTRAINT `ospos_permissions_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `ospos_stock_locations` (`location_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_permissions`
--

LOCK TABLES `ospos_permissions` WRITE;
/*!40000 ALTER TABLE `ospos_permissions` DISABLE KEYS */;
INSERT INTO `ospos_permissions` VALUES ('config','config',NULL),('credits','credits',NULL),('credits_admin','credits_admin',NULL),('customers','customers',NULL),('employees','employees',NULL),('expenses','expenses',NULL),('expenses_categories','expenses_categories',NULL),('giftcards','giftcards',NULL),('home','home',NULL),('items','items',NULL),('items_stock','items',1),('item_kits','item_kits',NULL),('messages','messages',NULL),('mpesa','mpesa',NULL),('mpesaadmin','mpesaadmin',NULL),('office','office',NULL),('receivings','receivings',NULL),('receivings_stock','receivings',1),('reports','reports',NULL),('reports_categories','reports',NULL),('reports_customers','reports',NULL),('reports_discounts','reports',NULL),('reports_employees','reports',NULL),('reports_expenses_categories','reports',NULL),('reports_inventory','reports',NULL),('reports_items','reports',NULL),('reports_payments','reports',NULL),('reports_receivings','reports',NULL),('reports_sales','reports',NULL),('reports_suppliers','reports',NULL),('reports_taxes','reports',NULL),('sales','sales',NULL),('sales_delete','sales',NULL),('sales_stock','sales',1),('suppliers','suppliers',NULL),('taxes','taxes',NULL);
/*!40000 ALTER TABLE `ospos_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_receivings`
--

DROP TABLE IF EXISTS `ospos_receivings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_receivings` (
  `receiving_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `supplier_id` int(10) DEFAULT NULL,
  `employee_id` int(10) NOT NULL DEFAULT '0',
  `comment` text,
  `receiving_id` int(10) NOT NULL AUTO_INCREMENT,
  `payment_type` varchar(20) DEFAULT NULL,
  `reference` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`receiving_id`),
  KEY `supplier_id` (`supplier_id`),
  KEY `employee_id` (`employee_id`),
  KEY `reference` (`reference`),
  CONSTRAINT `ospos_receivings_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `ospos_employees` (`person_id`),
  CONSTRAINT `ospos_receivings_ibfk_2` FOREIGN KEY (`supplier_id`) REFERENCES `ospos_suppliers` (`person_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_receivings`
--

LOCK TABLES `ospos_receivings` WRITE;
/*!40000 ALTER TABLE `ospos_receivings` DISABLE KEYS */;
INSERT INTO `ospos_receivings` VALUES ('2018-04-26 04:18:49',NULL,2,'My Comments',1,'Cash',NULL);
/*!40000 ALTER TABLE `ospos_receivings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_receivings_items`
--

DROP TABLE IF EXISTS `ospos_receivings_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_receivings_items` (
  `receiving_id` int(10) NOT NULL DEFAULT '0',
  `item_id` int(10) NOT NULL DEFAULT '0',
  `description` varchar(30) DEFAULT NULL,
  `serialnumber` varchar(30) DEFAULT NULL,
  `line` int(3) NOT NULL,
  `quantity_purchased` decimal(15,3) NOT NULL DEFAULT '0.000',
  `item_cost_price` decimal(15,2) NOT NULL,
  `item_unit_price` decimal(15,2) NOT NULL,
  `discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_location` int(11) NOT NULL,
  `receiving_quantity` decimal(15,3) NOT NULL DEFAULT '1.000',
  PRIMARY KEY (`receiving_id`,`item_id`,`line`),
  KEY `item_id` (`item_id`),
  CONSTRAINT `ospos_receivings_items_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `ospos_items` (`item_id`),
  CONSTRAINT `ospos_receivings_items_ibfk_2` FOREIGN KEY (`receiving_id`) REFERENCES `ospos_receivings` (`receiving_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_receivings_items`
--

LOCK TABLES `ospos_receivings_items` WRITE;
/*!40000 ALTER TABLE `ospos_receivings_items` DISABLE KEYS */;
INSERT INTO `ospos_receivings_items` VALUES (1,2,'','',1,1.000,15000.00,15000.00,0.00,1,1.000);
/*!40000 ALTER TABLE `ospos_receivings_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_sales`
--

DROP TABLE IF EXISTS `ospos_sales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_sales` (
  `sale_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `customer_id` int(10) DEFAULT NULL,
  `employee_id` int(10) NOT NULL DEFAULT '0',
  `comment` text,
  `invoice_number` varchar(32) DEFAULT NULL,
  `quote_number` varchar(32) DEFAULT NULL,
  `sale_id` int(10) NOT NULL AUTO_INCREMENT,
  `sale_status` tinyint(2) NOT NULL DEFAULT '0',
  `dinner_table_id` int(11) DEFAULT NULL,
  `work_order_number` varchar(32) DEFAULT NULL,
  `sale_type` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sale_id`),
  UNIQUE KEY `invoice_number` (`invoice_number`),
  KEY `customer_id` (`customer_id`),
  KEY `employee_id` (`employee_id`),
  KEY `sale_time` (`sale_time`),
  KEY `dinner_table_id` (`dinner_table_id`),
  CONSTRAINT `ospos_sales_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `ospos_employees` (`person_id`),
  CONSTRAINT `ospos_sales_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `ospos_customers` (`person_id`),
  CONSTRAINT `ospos_sales_ibfk_3` FOREIGN KEY (`dinner_table_id`) REFERENCES `ospos_dinner_tables` (`dinner_table_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_sales`
--

LOCK TABLES `ospos_sales` WRITE;
/*!40000 ALTER TABLE `ospos_sales` DISABLE KEYS */;
INSERT INTO `ospos_sales` VALUES ('2018-04-26 04:20:37',NULL,2,'','0',NULL,1,0,NULL,NULL,0);
/*!40000 ALTER TABLE `ospos_sales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_sales_items`
--

DROP TABLE IF EXISTS `ospos_sales_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_sales_items` (
  `sale_id` int(10) NOT NULL DEFAULT '0',
  `item_id` int(10) NOT NULL DEFAULT '0',
  `description` varchar(255) DEFAULT NULL,
  `serialnumber` varchar(30) DEFAULT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `quantity_purchased` decimal(15,3) NOT NULL DEFAULT '0.000',
  `item_cost_price` decimal(15,2) NOT NULL,
  `item_unit_price` decimal(15,2) NOT NULL,
  `discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_location` int(11) NOT NULL,
  `print_option` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sale_id`,`item_id`,`line`),
  KEY `sale_id` (`sale_id`),
  KEY `item_id` (`item_id`),
  KEY `item_location` (`item_location`),
  CONSTRAINT `ospos_sales_items_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `ospos_items` (`item_id`),
  CONSTRAINT `ospos_sales_items_ibfk_2` FOREIGN KEY (`sale_id`) REFERENCES `ospos_sales` (`sale_id`),
  CONSTRAINT `ospos_sales_items_ibfk_3` FOREIGN KEY (`item_location`) REFERENCES `ospos_stock_locations` (`location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_sales_items`
--

LOCK TABLES `ospos_sales_items` WRITE;
/*!40000 ALTER TABLE `ospos_sales_items` DISABLE KEYS */;
INSERT INTO `ospos_sales_items` VALUES (1,2,'','',1,10.000,15000.00,25000.00,0.00,1,0);
/*!40000 ALTER TABLE `ospos_sales_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_sales_items_taxes`
--

DROP TABLE IF EXISTS `ospos_sales_items_taxes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_sales_items_taxes` (
  `sale_id` int(10) NOT NULL,
  `item_id` int(10) NOT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `percent` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `tax_type` tinyint(2) NOT NULL DEFAULT '0',
  `rounding_code` tinyint(2) NOT NULL DEFAULT '0',
  `cascade_tax` tinyint(2) NOT NULL DEFAULT '0',
  `cascade_sequence` tinyint(2) NOT NULL DEFAULT '0',
  `item_tax_amount` decimal(15,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`sale_id`,`item_id`,`line`,`name`,`percent`),
  KEY `sale_id` (`sale_id`),
  KEY `item_id` (`item_id`),
  CONSTRAINT `ospos_sales_items_taxes_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `ospos_sales_items` (`sale_id`),
  CONSTRAINT `ospos_sales_items_taxes_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `ospos_items` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_sales_items_taxes`
--

LOCK TABLES `ospos_sales_items_taxes` WRITE;
/*!40000 ALTER TABLE `ospos_sales_items_taxes` DISABLE KEYS */;
/*!40000 ALTER TABLE `ospos_sales_items_taxes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_sales_payments`
--

DROP TABLE IF EXISTS `ospos_sales_payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_sales_payments` (
  `sale_id` int(10) NOT NULL,
  `payment_type` varchar(40) NOT NULL,
  `payment_amount` decimal(15,2) NOT NULL,
  PRIMARY KEY (`sale_id`,`payment_type`),
  KEY `sale_id` (`sale_id`),
  CONSTRAINT `ospos_sales_payments_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `ospos_sales` (`sale_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_sales_payments`
--

LOCK TABLES `ospos_sales_payments` WRITE;
/*!40000 ALTER TABLE `ospos_sales_payments` DISABLE KEYS */;
INSERT INTO `ospos_sales_payments` VALUES (1,'Cash',250000.00);
/*!40000 ALTER TABLE `ospos_sales_payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_sales_reward_points`
--

DROP TABLE IF EXISTS `ospos_sales_reward_points`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_sales_reward_points` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sale_id` int(11) NOT NULL,
  `earned` float NOT NULL,
  `used` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sale_id` (`sale_id`),
  CONSTRAINT `ospos_sales_reward_points_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `ospos_sales` (`sale_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_sales_reward_points`
--

LOCK TABLES `ospos_sales_reward_points` WRITE;
/*!40000 ALTER TABLE `ospos_sales_reward_points` DISABLE KEYS */;
/*!40000 ALTER TABLE `ospos_sales_reward_points` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_sales_taxes`
--

DROP TABLE IF EXISTS `ospos_sales_taxes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_sales_taxes` (
  `sale_id` int(10) NOT NULL,
  `tax_type` smallint(2) NOT NULL,
  `tax_group` varchar(32) NOT NULL,
  `sale_tax_basis` decimal(15,4) NOT NULL,
  `sale_tax_amount` decimal(15,4) NOT NULL,
  `print_sequence` tinyint(2) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `tax_rate` decimal(15,4) NOT NULL,
  `sales_tax_code` varchar(32) NOT NULL DEFAULT '',
  `rounding_code` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sale_id`,`tax_type`,`tax_group`),
  KEY `print_sequence` (`sale_id`,`print_sequence`,`tax_type`,`tax_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_sales_taxes`
--

LOCK TABLES `ospos_sales_taxes` WRITE;
/*!40000 ALTER TABLE `ospos_sales_taxes` DISABLE KEYS */;
/*!40000 ALTER TABLE `ospos_sales_taxes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_sessions`
--

DROP TABLE IF EXISTS `ospos_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_sessions`
--

LOCK TABLES `ospos_sessions` WRITE;
/*!40000 ALTER TABLE `ospos_sessions` DISABLE KEYS */;
INSERT INTO `ospos_sessions` VALUES ('c8s69n19kp6l6mlmrijimtjlg6bhr99s','::1',1526788102,'__ci_last_regenerate|i:1526787896;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('rv4o45kmcm641vj98j0ql5ijpshbnnkj','::1',1526788748,'__ci_last_regenerate|i:1526788288;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('313fggrhqo1eq6n5h54k77nfd5l11i42','::1',1526788984,'__ci_last_regenerate|i:1526788750;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('udlngrb93soc6f4pp0ig715198ppo36i','::1',1526789054,'__ci_last_regenerate|i:1526789052;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('goh4vb3ovc4esnfatt5qpdoogbtjnh8r','::1',1526789871,'__ci_last_regenerate|i:1526789578;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('e0nnnv4qi0mk0trbr1odjh6srkb1jfof','::1',1526790003,'__ci_last_regenerate|i:1526789902;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('oesmpie18k19p2kn3ieb0t4qb06i3ge0','::1',1526790656,'__ci_last_regenerate|i:1526790380;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('3o2opgki3rmda8hrqltndqmqqnsj8ama','::1',1526790793,'__ci_last_regenerate|i:1526790688;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('aucv4jt4fcqmqdunpjodlrg7jnef2vua','::1',1526791306,'__ci_last_regenerate|i:1526791295;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";recv_cart|a:0:{}recv_mode|s:7:\"receive\";recv_supplier|i:-1;'),('nhduc6d3m4kivel5c1f6gg8fqq3drc2s','::1',1529005992,'__ci_last_regenerate|i:1529005964;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('rbe2bp4126kc7lmm9fuubl7a0h0sn9k6','::1',1529007419,'__ci_last_regenerate|i:1529007375;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('etji0lbhvld0arohtl56h4rg82jvu0ap','::1',1529008180,'__ci_last_regenerate|i:1529008001;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('eqd35fthmnnhtvl75kp5kmatviq3l97i','::1',1529009476,'__ci_last_regenerate|i:1529009181;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('sksna0joa46knfjttuurh221ube2fnk2','::1',1529009795,'__ci_last_regenerate|i:1529009499;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('gm7s6a6i0v94b107hgh76g3v7ufu3rc7','::1',1529010108,'__ci_last_regenerate|i:1529009869;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('9rmr50pkf6dvdf84tg1bpd9tksbfacun','::1',1529010589,'__ci_last_regenerate|i:1529010452;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('32ldomqk5a6672itrht5noso100ob45u','::1',1529011069,'__ci_last_regenerate|i:1529010778;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('4vpmtb7hbe52c7tp1qi23cktdcjnaig0','::1',1529011344,'__ci_last_regenerate|i:1529011096;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('l7l3trtq758elpie6k0361ubo35i19mc','::1',1529011757,'__ci_last_regenerate|i:1529011481;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('8bgoudc650v7o81ieed94fsmbvlgs3bg','::1',1529012010,'__ci_last_regenerate|i:1529011820;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('df0j02ilqslpjitp8mdqsiat66iom9l0','::1',1529012790,'__ci_last_regenerate|i:1529012510;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('gfnoraeoqke92hllrkkm7bhjsbmqjige','::1',1529013115,'__ci_last_regenerate|i:1529012831;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('i9fn77sandjt4ud5j6jc206m7eopesob','::1',1529012831,'__ci_last_regenerate|i:1529012831;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('jld75vot7fjk9jjrqhcquk5gttunnmds','::1',1529013533,'__ci_last_regenerate|i:1529013237;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('1t53jocp25vh5ofn1plrbgrn96p6ttu6','::1',1529013873,'__ci_last_regenerate|i:1529013591;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('0e9eb0ol7q0u7qr0ig4eroellu4rr7sv','::1',1529014271,'__ci_last_regenerate|i:1529014047;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('vlcd008np0rho44e0pkqoupdau6525k1','::1',1529014504,'__ci_last_regenerate|i:1529014369;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('f9sjm8mjquvalg9l56e6dvv5sqri84r5','::1',1529014919,'__ci_last_regenerate|i:1529014678;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('sgvrpdsml0gg8kaqk249uetjrem73fs6','::1',1529015254,'__ci_last_regenerate|i:1529014980;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('f4r4f96tnmi193vuirapfoi0vo4umcpv','::1',1529015588,'__ci_last_regenerate|i:1529015306;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('pk5brtjdg2t274kkgnotcj26vnsfiadl','::1',1529015995,'__ci_last_regenerate|i:1529015711;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('bkjcepogflkll67le861p35p41soigu9','::1',1529016240,'__ci_last_regenerate|i:1529016053;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('cnvsete99793p33o2q4jpeb98a53qqji','::1',1529016504,'__ci_last_regenerate|i:1529016503;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('qgr9h4uh721bea46usas0ij1fdl8r675','::1',1529016884,'__ci_last_regenerate|i:1529016845;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('jp0t2pokg6pv0rbob630qci91qhreinj','::1',1529017339,'__ci_last_regenerate|i:1529017254;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('iarj8qnipvhioabbqgm5gq2pk99e70e3','::1',1529017666,'__ci_last_regenerate|i:1529017568;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('vgap6icpea6d2qorl6k17o3dvv830air','::1',1529018272,'__ci_last_regenerate|i:1529017996;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('kr8fhk3in7mp6daen7incforhs2re5or','::1',1529018340,'__ci_last_regenerate|i:1529018299;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('cfuq8nbs8dnp4jbi8evuqpnb1uurt76p','::1',1529019215,'__ci_last_regenerate|i:1529018983;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('pbu9l269r8i5bsptobvnr9pv556svhmq','::1',1529019683,'__ci_last_regenerate|i:1529019428;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('au0toq3b8qsekmhqt1k1lfa8vi009qj6','::1',1529020553,'__ci_last_regenerate|i:1529020260;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('7u41ccgq9q309mc3cc75gk0lmoi3127h','::1',1529021480,'__ci_last_regenerate|i:1529021186;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('lrsaqeg188ko6br4q7fq08iuk2bfnrm7','::1',1529021750,'__ci_last_regenerate|i:1529021563;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('8k3go5oe1mfneh6uekcu4mqk1r5ttoen','::1',1529022135,'__ci_last_regenerate|i:1529021865;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('qaog7dhhrr9a0ik4mdiuoe2ui9ir85ln','::1',1529022460,'__ci_last_regenerate|i:1529022171;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('c70a1b1t2j302febr9me09i8i6bfd6pv','::1',1529022893,'__ci_last_regenerate|i:1529022632;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('v8qg218tsr1f9hf3cgsbbdeot68otp8f','::1',1529023216,'__ci_last_regenerate|i:1529023033;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('v2d8j29ngnejrna1mfq9k0f8n6vrdpdk','::1',1529023831,'__ci_last_regenerate|i:1529023827;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('vbskhfo673irlso35o7oc1k4v4umst32','::1',1529071980,'session_sha1|s:7:\"4f5ad57\";__ci_last_regenerate|i:1529071703;person_id|s:1:\"2\";'),('hvjh9beqndrom0isbpar93n418kc4717','::1',1529072058,'session_sha1|s:7:\"4f5ad57\";__ci_last_regenerate|i:1529072048;person_id|s:1:\"2\";'),('gap13tk0p3ls3t2c3qt093e1vq4uorp2','::1',1529072210,'__ci_last_regenerate|i:1529072114;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('d4lg7cjc3o36vl28r5le70oukfc1q04a','::1',1529072876,'session_sha1|s:7:\"4f5ad57\";__ci_last_regenerate|i:1529072696;person_id|s:1:\"2\";'),('meb524qh7g6lb8s3b6jfl1a6pnv0kpss','::1',1529073173,'session_sha1|s:7:\"4f5ad57\";__ci_last_regenerate|i:1529073049;person_id|s:1:\"2\";'),('pga7o59hdss9uaojrrvkbqj8hue6oo6b','::1',1529073843,'session_sha1|s:7:\"4f5ad57\";__ci_last_regenerate|i:1529073753;person_id|s:1:\"2\";'),('eie6crm1ekpr7ivk3ji9i5o1k41pfe0o','::1',1529074729,'session_sha1|s:7:\"4f5ad57\";__ci_last_regenerate|i:1529074446;person_id|s:1:\"2\";'),('l0e5lighlp4tkurd482qq64n9ujd5jlu','::1',1529074895,'session_sha1|s:7:\"4f5ad57\";__ci_last_regenerate|i:1529074752;person_id|s:1:\"2\";'),('bk3lgluvp4rt14r0dv8dcnk610asbcrf','::1',1529077422,'session_sha1|s:7:\"4f5ad57\";__ci_last_regenerate|i:1529077421;person_id|s:1:\"2\";'),('7nugramcotkqg3qi0gl48t1e6ouoherh','::1',1529116511,'__ci_last_regenerate|i:1529116491;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('561oh1d34p0an3731dnpaps5sdmeep8j','::1',1529117118,'__ci_last_regenerate|i:1529116854;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('cp1upobjlpctvajrv99q8k3u8b6t15m2','::1',1529117411,'__ci_last_regenerate|i:1529117157;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('tr7l92nn8ar28dmo9nq07cld0cdh3h7q','::1',1529117724,'__ci_last_regenerate|i:1529117468;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('p6nnbttq4jnaae6pcj0drp3tdb7lpcgb','::1',1529118295,'__ci_last_regenerate|i:1529118004;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('r29vu99b1gd1anttbbm1qm23ffl0d6s8','::1',1529118600,'__ci_last_regenerate|i:1529118311;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('ai6d343spithdo07pekijcbepk118mko','::1',1529118907,'__ci_last_regenerate|i:1529118614;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('7ikunrrvch45j4ifvpal7p4vnvhilp7t','::1',1529119588,'__ci_last_regenerate|i:1529119341;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('q3nomplo8js2sgon3s46m6egi0car8hv','::1',1529119787,'__ci_last_regenerate|i:1529119784;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('ns9j431d4k6sif6k709ppf6c2lkb8ngd','::1',1529120241,'__ci_last_regenerate|i:1529120182;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('9187337h0bfvo7qbkov2b4bq3nrd4127','::1',1529120704,'__ci_last_regenerate|i:1529120503;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('pa5o3pgr98jkukt96088ugrjbd325rtj','::1',1529121112,'__ci_last_regenerate|i:1529120903;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('jats84ba99209g8q73q0786je8fpl44n','::1',1529122032,'__ci_last_regenerate|i:1529121744;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('dfj86db7f0eim58lqflhpqnfrgem87f5','::1',1529122054,'__ci_last_regenerate|i:1529122053;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('dqvv59fm1o3s2nblrlif1bjr1ag86bv5','::1',1529123662,'__ci_last_regenerate|i:1529123432;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('9gtfu6dk550gj3s8hrfdlqb2o8d39517','::1',1529123846,'__ci_last_regenerate|i:1529123780;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('usocu84sa4e2sagse60fnmdig6cf54mu','::1',1529124371,'__ci_last_regenerate|i:1529124103;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('2tr26sesaibrs00mk8d67thp2neulv2l','::1',1529124666,'__ci_last_regenerate|i:1529124488;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('4rd65561f2rvdlef6j50juha9rpmoao3','::1',1529125104,'__ci_last_regenerate|i:1529124806;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('nuenek0rp43p3snlvd5a2kmm57c34pco','::1',1529125309,'__ci_last_regenerate|i:1529125107;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('4fkh7acggmgtijhoigl71rl0rvij3mkn','::1',1529125699,'__ci_last_regenerate|i:1529125618;session_sha1|s:7:\"4f5ad57\";'),('r1obipfpb4ddb3djdaa402hn3v139bmi','::1',1529150552,'__ci_last_regenerate|i:1529150396;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('cv7ra7223onn57ot7deje44k6g3mqo1u','::1',1529151043,'__ci_last_regenerate|i:1529150743;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('jitvrjj0s4havn447suc342doggpvl2p','::1',1529151130,'__ci_last_regenerate|i:1529151077;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('lokfkhdi2kiva1fci21mjh7jbv7c3kmb','::1',1529151705,'__ci_last_regenerate|i:1529151546;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('99cdsrn78n71enkkdn6b215un2ic10mt','::1',1529151932,'__ci_last_regenerate|i:1529151931;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('30k2k9hr4m7s6lgg3ub1kk86t7182d69','::1',1529152336,'__ci_last_regenerate|i:1529152291;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('rned6hd3vgd95j35telt7a05bo2fndrs','::1',1529153944,'__ci_last_regenerate|i:1529153644;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('mj6ev168v40v2br7qanp1ihns3r30lbq','::1',1529154015,'__ci_last_regenerate|i:1529153946;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('hv6u5tg7n7dv1okj4j9oa2ludin4fn49','::1',1529154665,'__ci_last_regenerate|i:1529154663;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('o77g32h5c8flaihhd6bc8o06jacudv9h','::1',1529158931,'__ci_last_regenerate|i:1529158664;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('nf0mtiffqo4738n835k8sdhihvkebf9e','::1',1529159364,'__ci_last_regenerate|i:1529159071;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('nvmugq1pe1efbr19rs4uvkmsh7u0f4fq','::1',1529344124,'__ci_last_regenerate|i:1529344101;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('5br3a04hir8okitl5qfqjgq8j9mmgfhc','::1',1529344706,'__ci_last_regenerate|i:1529344515;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";recv_cart|a:0:{}recv_mode|s:6:\"return\";recv_supplier|i:-1;'),('kl5b15sufln4oojia2hitod3io77lem2','::1',1529348586,'__ci_last_regenerate|i:1529348447;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";recv_cart|a:0:{}recv_mode|s:6:\"return\";recv_supplier|i:-1;'),('l3qaij2debemb80mabqvnms0v2g48nib','::1',1529349024,'__ci_last_regenerate|i:1529348902;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";recv_cart|a:0:{}recv_mode|s:6:\"return\";recv_supplier|i:-1;'),('iv831g5kk7k0ouq8c02ic8onbsou46sn','::1',1529349570,'__ci_last_regenerate|i:1529349325;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";recv_cart|a:0:{}recv_mode|s:6:\"return\";recv_supplier|i:-1;'),('gevc5ktpvl89u8fcquttk9req5gbvgl5','::1',1529350456,'__ci_last_regenerate|i:1529350446;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";recv_cart|a:0:{}recv_mode|s:6:\"return\";recv_supplier|i:-1;');
/*!40000 ALTER TABLE `ospos_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_stock_locations`
--

DROP TABLE IF EXISTS `ospos_stock_locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_stock_locations` (
  `location_id` int(11) NOT NULL AUTO_INCREMENT,
  `location_name` varchar(255) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`location_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_stock_locations`
--

LOCK TABLES `ospos_stock_locations` WRITE;
/*!40000 ALTER TABLE `ospos_stock_locations` DISABLE KEYS */;
INSERT INTO `ospos_stock_locations` VALUES (1,'stock',0),(2,'stock',0),(3,'stock',0);
/*!40000 ALTER TABLE `ospos_stock_locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_suppliers`
--

DROP TABLE IF EXISTS `ospos_suppliers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_suppliers` (
  `person_id` int(10) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `agency_name` varchar(255) NOT NULL,
  `account_number` varchar(255) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `account_number` (`account_number`),
  KEY `person_id` (`person_id`),
  CONSTRAINT `ospos_suppliers_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `ospos_people` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_suppliers`
--

LOCK TABLES `ospos_suppliers` WRITE;
/*!40000 ALTER TABLE `ospos_suppliers` DISABLE KEYS */;
/*!40000 ALTER TABLE `ospos_suppliers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_tax_categories`
--

DROP TABLE IF EXISTS `ospos_tax_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_tax_categories` (
  `tax_category_id` int(10) NOT NULL AUTO_INCREMENT,
  `tax_category` varchar(32) NOT NULL,
  `tax_group_sequence` tinyint(2) NOT NULL,
  PRIMARY KEY (`tax_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_tax_categories`
--

LOCK TABLES `ospos_tax_categories` WRITE;
/*!40000 ALTER TABLE `ospos_tax_categories` DISABLE KEYS */;
INSERT INTO `ospos_tax_categories` VALUES (1,'Standard',10),(2,'Service',12),(3,'Alcohol',11),(4,'Standard',10),(5,'Service',12),(6,'Alcohol',11),(7,'Standard',10),(8,'Service',12),(9,'Alcohol',11);
/*!40000 ALTER TABLE `ospos_tax_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_tax_code_rates`
--

DROP TABLE IF EXISTS `ospos_tax_code_rates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_tax_code_rates` (
  `rate_tax_code` varchar(32) NOT NULL,
  `rate_tax_category_id` int(10) NOT NULL,
  `tax_rate` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `rounding_code` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`rate_tax_code`,`rate_tax_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_tax_code_rates`
--

LOCK TABLES `ospos_tax_code_rates` WRITE;
/*!40000 ALTER TABLE `ospos_tax_code_rates` DISABLE KEYS */;
/*!40000 ALTER TABLE `ospos_tax_code_rates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_tax_codes`
--

DROP TABLE IF EXISTS `ospos_tax_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_tax_codes` (
  `tax_code` varchar(32) NOT NULL,
  `tax_code_name` varchar(255) NOT NULL DEFAULT '',
  `tax_code_type` tinyint(2) NOT NULL DEFAULT '0',
  `city` varchar(255) NOT NULL DEFAULT '',
  `state` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`tax_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_tax_codes`
--

LOCK TABLES `ospos_tax_codes` WRITE;
/*!40000 ALTER TABLE `ospos_tax_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `ospos_tax_codes` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-22 15:24:15
