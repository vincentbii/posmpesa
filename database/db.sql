-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: opensourcepos
-- ------------------------------------------------------
-- Server version	5.7.19-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ospos_app_config`
--

DROP TABLE IF EXISTS `ospos_app_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_app_config` (
  `key` varchar(50) NOT NULL,
  `value` varchar(500) NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_app_config`
--

LOCK TABLES `ospos_app_config` WRITE;
/*!40000 ALTER TABLE `ospos_app_config` DISABLE KEYS */;
INSERT INTO `ospos_app_config` VALUES ('address','P.O Box XXXX'),('allow_duplicate_barcodes','0'),('barcode_content','id'),('barcode_first_row','category'),('barcode_font','Arial'),('barcode_font_size','10'),('barcode_formats','[]'),('barcode_generate_if_empty','0'),('barcode_height','50'),('barcode_num_in_row','2'),('barcode_page_cellspacing','20'),('barcode_page_width','100'),('barcode_second_row','item_code'),('barcode_third_row','unit_price'),('barcode_type','Code39'),('barcode_width','250'),('cash_decimals','2'),('cash_rounding_code','0'),('client_id','d57e0ab6-4607-4171-885b-50494e03e402'),('company','Eclipse Consultants'),('company_logo','company_logo.png'),('country_codes','us'),('currency_decimals','2'),('currency_symbol','$'),('customer_reward_enable','0'),('customer_sales_tax_support','0'),('dateformat','m/d/Y'),('date_or_time_format',''),('default_origin_tax_code',''),('default_register_mode','sale'),('default_sales_discount','0'),('default_tax_category','Standard'),('derive_sale_quantity','0'),('dinner_table_enable','0'),('email',''),('email_receipt_check_behaviour','last'),('fax',''),('financial_year','1'),('gcaptcha_enable','0'),('gcaptcha_secret_key',''),('gcaptcha_site_key',''),('giftcard_number','series'),('invoice_default_comments','This is a default comment'),('invoice_email_message','Dear {CU}, In attachment the receipt for sale {ISEQ}'),('invoice_enable','1'),('language','english'),('language_code','en-US'),('last_used_invoice_number','0'),('last_used_quote_number','0'),('last_used_work_order_number','0'),('lines_per_page','25'),('line_sequence','0'),('mailpath','/usr/sbin/sendmail'),('msg_msg',''),('msg_pwd',''),('msg_src',''),('msg_uid',''),('notify_horizontal_position','center'),('notify_vertical_position','bottom'),('number_locale','en_US'),('payment_options_order','cashdebitcredit'),('phone','0770369799'),('print_bottom_margin','0'),('print_footer','0'),('print_header','0'),('print_left_margin','0'),('print_receipt_check_behaviour','last'),('print_right_margin','0'),('print_silently','1'),('print_top_margin','0'),('protocol','mail'),('quantity_decimals','0'),('quote_default_comments','This is a default quote comment'),('receipt_font_size','12'),('receipt_show_company_name','1'),('receipt_show_description','1'),('receipt_show_serialnumber','1'),('receipt_show_taxes','0'),('receipt_show_total_discount','1'),('receipt_template','receipt_default'),('receiving_calculate_average_price','0'),('recv_invoice_format','{CO}'),('return_policy','Return Policy'),('sales_invoice_format','{CO}'),('sales_quote_format','Q%y{QSEQ:6}'),('smtp_crypto','ssl'),('smtp_host',''),('smtp_pass',''),('smtp_port','465'),('smtp_timeout','5'),('smtp_user',''),('suggestions_first_column','name'),('suggestions_second_column',''),('suggestions_third_column',''),('tax_decimals','2'),('tax_included','0'),('theme','flatly'),('thousands_separator','1'),('timeformat','H:i:s'),('timezone','America/New_York'),('website',''),('work_order_enable','0'),('work_order_format','W%y{WSEQ:6}');
/*!40000 ALTER TABLE `ospos_app_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_customers`
--

DROP TABLE IF EXISTS `ospos_customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_customers` (
  `person_id` int(10) NOT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `account_number` varchar(255) DEFAULT NULL,
  `taxable` int(1) NOT NULL DEFAULT '1',
  `sales_tax_code` varchar(32) NOT NULL DEFAULT '1',
  `discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `package_id` int(11) DEFAULT NULL,
  `points` int(11) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `account_number` (`account_number`),
  KEY `person_id` (`person_id`),
  KEY `package_id` (`package_id`),
  CONSTRAINT `ospos_customers_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `ospos_people` (`person_id`),
  CONSTRAINT `ospos_customers_ibfk_2` FOREIGN KEY (`package_id`) REFERENCES `ospos_customers_packages` (`package_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_customers`
--

LOCK TABLES `ospos_customers` WRITE;
/*!40000 ALTER TABLE `ospos_customers` DISABLE KEYS */;
/*!40000 ALTER TABLE `ospos_customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_customers_packages`
--

DROP TABLE IF EXISTS `ospos_customers_packages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_customers_packages` (
  `package_id` int(11) NOT NULL AUTO_INCREMENT,
  `package_name` varchar(255) DEFAULT NULL,
  `points_percent` float NOT NULL DEFAULT '0',
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`package_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_customers_packages`
--

LOCK TABLES `ospos_customers_packages` WRITE;
/*!40000 ALTER TABLE `ospos_customers_packages` DISABLE KEYS */;
INSERT INTO `ospos_customers_packages` VALUES (1,'Default',0,0),(2,'Bronze',10,0),(3,'Silver',20,0),(4,'Gold',30,0),(5,'Premium',50,0),(6,'Default',0,0),(7,'Bronze',10,0),(8,'Silver',20,0),(9,'Gold',30,0),(10,'Premium',50,0),(11,'Default',0,0),(12,'Bronze',10,0),(13,'Silver',20,0),(14,'Gold',30,0),(15,'Premium',50,0);
/*!40000 ALTER TABLE `ospos_customers_packages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_customers_points`
--

DROP TABLE IF EXISTS `ospos_customers_points`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_customers_points` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `sale_id` int(11) NOT NULL,
  `points_earned` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`),
  KEY `package_id` (`package_id`),
  KEY `sale_id` (`sale_id`),
  CONSTRAINT `ospos_customers_points_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `ospos_customers` (`person_id`),
  CONSTRAINT `ospos_customers_points_ibfk_2` FOREIGN KEY (`package_id`) REFERENCES `ospos_customers_packages` (`package_id`),
  CONSTRAINT `ospos_customers_points_ibfk_3` FOREIGN KEY (`sale_id`) REFERENCES `ospos_sales` (`sale_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_customers_points`
--

LOCK TABLES `ospos_customers_points` WRITE;
/*!40000 ALTER TABLE `ospos_customers_points` DISABLE KEYS */;
/*!40000 ALTER TABLE `ospos_customers_points` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_dinner_tables`
--

DROP TABLE IF EXISTS `ospos_dinner_tables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_dinner_tables` (
  `dinner_table_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`dinner_table_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_dinner_tables`
--

LOCK TABLES `ospos_dinner_tables` WRITE;
/*!40000 ALTER TABLE `ospos_dinner_tables` DISABLE KEYS */;
INSERT INTO `ospos_dinner_tables` VALUES (1,'Delivery',0,0),(2,'Take Away',0,0),(3,'Delivery',0,0),(4,'Take Away',0,0),(5,'Delivery',0,0),(6,'Take Away',0,0);
/*!40000 ALTER TABLE `ospos_dinner_tables` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_employees`
--

DROP TABLE IF EXISTS `ospos_employees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_employees` (
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `person_id` int(10) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `hash_version` int(1) NOT NULL DEFAULT '2',
  `language` varchar(48) DEFAULT NULL,
  `language_code` varchar(8) DEFAULT NULL,
  UNIQUE KEY `username` (`username`),
  KEY `person_id` (`person_id`),
  CONSTRAINT `ospos_employees_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `ospos_people` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_employees`
--

LOCK TABLES `ospos_employees` WRITE;
/*!40000 ALTER TABLE `ospos_employees` DISABLE KEYS */;
INSERT INTO `ospos_employees` VALUES ('admin','$2y$10$vJBSMlD02EC7ENSrKfVQXuvq9tNRHMtcOA8MSK2NYS748HHWm.gcG',1,0,2,NULL,NULL),('vincent','$2y$10$3w7D1U7FyMDwlDMaK.7DaO2Gj8AF7.Q3cSnlclE6sWnPqYCGzaaCm',2,0,2,NULL,NULL);
/*!40000 ALTER TABLE `ospos_employees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_expense_categories`
--

DROP TABLE IF EXISTS `ospos_expense_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_expense_categories` (
  `expense_category_id` int(10) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) DEFAULT NULL,
  `category_description` varchar(255) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`expense_category_id`),
  UNIQUE KEY `category_name` (`category_name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_expense_categories`
--

LOCK TABLES `ospos_expense_categories` WRITE;
/*!40000 ALTER TABLE `ospos_expense_categories` DISABLE KEYS */;
INSERT INTO `ospos_expense_categories` VALUES (1,'Utilities','Water',0);
/*!40000 ALTER TABLE `ospos_expense_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_expenses`
--

DROP TABLE IF EXISTS `ospos_expenses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_expenses` (
  `expense_id` int(10) NOT NULL AUTO_INCREMENT,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `amount` decimal(15,2) NOT NULL,
  `payment_type` varchar(40) NOT NULL,
  `expense_category_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `employee_id` int(10) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `supplier_name` varchar(255) DEFAULT NULL,
  `supplier_tax_code` varchar(255) DEFAULT NULL,
  `tax_amount` decimal(15,2) DEFAULT NULL,
  PRIMARY KEY (`expense_id`),
  KEY `expense_category_id` (`expense_category_id`),
  KEY `employee_id` (`employee_id`),
  CONSTRAINT `ospos_expenses_ibfk_1` FOREIGN KEY (`expense_category_id`) REFERENCES `ospos_expense_categories` (`expense_category_id`),
  CONSTRAINT `ospos_expenses_ibfk_2` FOREIGN KEY (`employee_id`) REFERENCES `ospos_employees` (`person_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_expenses`
--

LOCK TABLES `ospos_expenses` WRITE;
/*!40000 ALTER TABLE `ospos_expenses` DISABLE KEYS */;
INSERT INTO `ospos_expenses` VALUES (1,'2017-04-20 04:00:00',15.00,'',1,'Water',1,0,NULL,NULL,NULL);
/*!40000 ALTER TABLE `ospos_expenses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_giftcards`
--

DROP TABLE IF EXISTS `ospos_giftcards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_giftcards` (
  `record_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `giftcard_id` int(11) NOT NULL AUTO_INCREMENT,
  `giftcard_number` varchar(255) DEFAULT NULL,
  `value` decimal(15,2) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `person_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`giftcard_id`),
  UNIQUE KEY `giftcard_number` (`giftcard_number`),
  KEY `person_id` (`person_id`),
  CONSTRAINT `ospos_giftcards_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `ospos_people` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_giftcards`
--

LOCK TABLES `ospos_giftcards` WRITE;
/*!40000 ALTER TABLE `ospos_giftcards` DISABLE KEYS */;
/*!40000 ALTER TABLE `ospos_giftcards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_grants`
--

DROP TABLE IF EXISTS `ospos_grants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_grants` (
  `permission_id` varchar(255) NOT NULL,
  `person_id` int(10) NOT NULL,
  `menu_group` varchar(32) DEFAULT 'home',
  PRIMARY KEY (`permission_id`,`person_id`),
  KEY `ospos_grants_ibfk_2` (`person_id`),
  CONSTRAINT `ospos_grants_ibfk_1` FOREIGN KEY (`permission_id`) REFERENCES `ospos_permissions` (`permission_id`) ON DELETE CASCADE,
  CONSTRAINT `ospos_grants_ibfk_2` FOREIGN KEY (`person_id`) REFERENCES `ospos_employees` (`person_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_grants`
--

LOCK TABLES `ospos_grants` WRITE;
/*!40000 ALTER TABLE `ospos_grants` DISABLE KEYS */;
INSERT INTO `ospos_grants` VALUES ('config',1,'office'),('customers',1,'home'),('customers',2,'home'),('employees',1,'office'),('expenses',1,'home'),('expenses_categories',1,'home'),('giftcards',1,'home'),('home',1,'office'),('home',2,'home'),('items',1,'home'),('items',2,'home'),('items_stock',1,'home'),('items_stock',2,'home'),('item_kits',1,'home'),('item_kits',2,'home'),('messages',1,'home'),('mpesa',2,'home'),('mpesaadmin',2,'home'),('office',1,'home'),('receivings',1,'home'),('receivings',2,'home'),('receivings_stock',1,'home'),('receivings_stock',2,'home'),('reports',1,'home'),('reports',2,'home'),('reports_categories',1,'home'),('reports_categories',2,'home'),('reports_customers',1,'home'),('reports_discounts',1,'home'),('reports_employees',1,'home'),('reports_employees',2,'home'),('reports_expenses_categories',1,'home'),('reports_expenses_categories',2,'home'),('reports_inventory',1,'home'),('reports_inventory',2,'home'),('reports_items',1,'home'),('reports_items',2,'home'),('reports_payments',1,'home'),('reports_payments',2,'home'),('reports_receivings',1,'home'),('reports_receivings',2,'home'),('reports_sales',1,'home'),('reports_sales',2,'home'),('reports_suppliers',1,'home'),('reports_suppliers',2,'home'),('reports_taxes',1,'home'),('reports_taxes',2,'home'),('sales',1,'home'),('sales',2,'home'),('sales_delete',1,'--'),('sales_delete',2,'home'),('sales_stock',1,'home'),('sales_stock',2,'home'),('suppliers',1,'home'),('taxes',1,'office');
/*!40000 ALTER TABLE `ospos_grants` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_inventory`
--

DROP TABLE IF EXISTS `ospos_inventory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_inventory` (
  `trans_id` int(11) NOT NULL AUTO_INCREMENT,
  `trans_items` int(11) NOT NULL DEFAULT '0',
  `trans_user` int(11) NOT NULL DEFAULT '0',
  `trans_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `trans_comment` text NOT NULL,
  `trans_location` int(11) NOT NULL,
  `trans_inventory` decimal(15,3) NOT NULL DEFAULT '0.000',
  PRIMARY KEY (`trans_id`),
  KEY `trans_items` (`trans_items`),
  KEY `trans_user` (`trans_user`),
  KEY `trans_location` (`trans_location`),
  CONSTRAINT `ospos_inventory_ibfk_1` FOREIGN KEY (`trans_items`) REFERENCES `ospos_items` (`item_id`),
  CONSTRAINT `ospos_inventory_ibfk_2` FOREIGN KEY (`trans_user`) REFERENCES `ospos_employees` (`person_id`),
  CONSTRAINT `ospos_inventory_ibfk_3` FOREIGN KEY (`trans_location`) REFERENCES `ospos_stock_locations` (`location_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_inventory`
--

LOCK TABLES `ospos_inventory` WRITE;
/*!40000 ALTER TABLE `ospos_inventory` DISABLE KEYS */;
INSERT INTO `ospos_inventory` VALUES (1,1,2,'2018-04-26 04:17:05','Manual Edit of Quantity',1,1000.000),(2,2,2,'2018-04-26 04:18:20','Manual Edit of Quantity',1,100.000),(3,2,2,'2018-04-26 04:18:49','RECV 1',1,1.000),(4,2,2,'2018-04-26 04:20:37','POS 1',1,-10.000);
/*!40000 ALTER TABLE `ospos_inventory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_item_kit_items`
--

DROP TABLE IF EXISTS `ospos_item_kit_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_item_kit_items` (
  `item_kit_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `quantity` decimal(15,3) NOT NULL,
  `kit_sequence` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`item_kit_id`,`item_id`,`quantity`),
  KEY `ospos_item_kit_items_ibfk_2` (`item_id`),
  CONSTRAINT `ospos_item_kit_items_ibfk_1` FOREIGN KEY (`item_kit_id`) REFERENCES `ospos_item_kits` (`item_kit_id`) ON DELETE CASCADE,
  CONSTRAINT `ospos_item_kit_items_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `ospos_items` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_item_kit_items`
--

LOCK TABLES `ospos_item_kit_items` WRITE;
/*!40000 ALTER TABLE `ospos_item_kit_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `ospos_item_kit_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_item_kits`
--

DROP TABLE IF EXISTS `ospos_item_kits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_item_kits` (
  `item_kit_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `item_id` int(10) NOT NULL DEFAULT '0',
  `kit_discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `price_option` tinyint(2) NOT NULL DEFAULT '0',
  `print_option` tinyint(2) NOT NULL DEFAULT '0',
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`item_kit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_item_kits`
--

LOCK TABLES `ospos_item_kits` WRITE;
/*!40000 ALTER TABLE `ospos_item_kits` DISABLE KEYS */;
INSERT INTO `ospos_item_kits` VALUES (1,'Kit One',0,0.00,0,0,'');
/*!40000 ALTER TABLE `ospos_item_kits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_item_quantities`
--

DROP TABLE IF EXISTS `ospos_item_quantities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_item_quantities` (
  `item_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `quantity` decimal(15,3) NOT NULL DEFAULT '0.000',
  PRIMARY KEY (`item_id`,`location_id`),
  KEY `item_id` (`item_id`),
  KEY `location_id` (`location_id`),
  CONSTRAINT `ospos_item_quantities_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `ospos_items` (`item_id`),
  CONSTRAINT `ospos_item_quantities_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `ospos_stock_locations` (`location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_item_quantities`
--

LOCK TABLES `ospos_item_quantities` WRITE;
/*!40000 ALTER TABLE `ospos_item_quantities` DISABLE KEYS */;
INSERT INTO `ospos_item_quantities` VALUES (1,1,1000.000),(2,1,91.000);
/*!40000 ALTER TABLE `ospos_item_quantities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_items`
--

DROP TABLE IF EXISTS `ospos_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_items` (
  `name` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `item_number` varchar(255) DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  `cost_price` decimal(15,2) NOT NULL,
  `unit_price` decimal(15,2) NOT NULL,
  `reorder_level` decimal(15,3) NOT NULL DEFAULT '0.000',
  `receiving_quantity` decimal(15,3) NOT NULL DEFAULT '1.000',
  `item_id` int(10) NOT NULL AUTO_INCREMENT,
  `pic_filename` varchar(255) DEFAULT NULL,
  `allow_alt_description` tinyint(1) NOT NULL,
  `is_serialized` tinyint(1) NOT NULL,
  `stock_type` tinyint(2) NOT NULL DEFAULT '0',
  `item_type` tinyint(2) NOT NULL DEFAULT '0',
  `tax_category_id` int(10) NOT NULL DEFAULT '1',
  `deleted` int(1) NOT NULL DEFAULT '0',
  `custom1` varchar(255) DEFAULT NULL,
  `custom2` varchar(255) DEFAULT NULL,
  `custom3` varchar(255) DEFAULT NULL,
  `custom4` varchar(255) DEFAULT NULL,
  `custom5` varchar(255) DEFAULT NULL,
  `custom6` varchar(255) DEFAULT NULL,
  `custom7` varchar(255) DEFAULT NULL,
  `custom8` varchar(255) DEFAULT NULL,
  `custom9` varchar(255) DEFAULT NULL,
  `custom10` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  KEY `item_number` (`item_number`),
  KEY `supplier_id` (`supplier_id`),
  CONSTRAINT `ospos_items_ibfk_1` FOREIGN KEY (`supplier_id`) REFERENCES `ospos_suppliers` (`person_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_items`
--

LOCK TABLES `ospos_items` WRITE;
/*!40000 ALTER TABLE `ospos_items` DISABLE KEYS */;
INSERT INTO `ospos_items` VALUES ('Phone','1',NULL,NULL,'',14000.00,20000.00,100.000,1000.000,1,NULL,0,0,0,0,0,0,'','','','','','','','','',''),('Samsang Calaxy','phones',NULL,NULL,'',15000.00,25000.00,1.000,1.000,2,NULL,0,0,0,0,0,0,'','','','','','','','','','');
/*!40000 ALTER TABLE `ospos_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_items_taxes`
--

DROP TABLE IF EXISTS `ospos_items_taxes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_items_taxes` (
  `item_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `percent` decimal(15,3) NOT NULL,
  PRIMARY KEY (`item_id`,`name`,`percent`),
  CONSTRAINT `ospos_items_taxes_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `ospos_items` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_items_taxes`
--

LOCK TABLES `ospos_items_taxes` WRITE;
/*!40000 ALTER TABLE `ospos_items_taxes` DISABLE KEYS */;
/*!40000 ALTER TABLE `ospos_items_taxes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_migrations`
--

DROP TABLE IF EXISTS `ospos_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_migrations`
--

LOCK TABLES `ospos_migrations` WRITE;
/*!40000 ALTER TABLE `ospos_migrations` DISABLE KEYS */;
INSERT INTO `ospos_migrations` VALUES (20180225100000);
/*!40000 ALTER TABLE `ospos_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_modules`
--

DROP TABLE IF EXISTS `ospos_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_modules` (
  `name_lang_key` varchar(255) NOT NULL,
  `desc_lang_key` varchar(255) NOT NULL,
  `sort` int(10) NOT NULL,
  `module_id` varchar(255) NOT NULL,
  PRIMARY KEY (`module_id`),
  UNIQUE KEY `desc_lang_key` (`desc_lang_key`),
  UNIQUE KEY `name_lang_key` (`name_lang_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_modules`
--

LOCK TABLES `ospos_modules` WRITE;
/*!40000 ALTER TABLE `ospos_modules` DISABLE KEYS */;
INSERT INTO `ospos_modules` VALUES ('module_config','module_config_desc',110,'config'),('module_customers','module_customers_desc',10,'customers'),('module_employees','module_employees_desc',80,'employees'),('module_expenses','module_expenses_desc',108,'expenses'),('module_expenses_categories','module_expenses_categories_desc',109,'expenses_categories'),('module_giftcards','module_giftcards_desc',90,'giftcards'),('module_home','module_home_desc',1,'home'),('module_items','module_items_desc',20,'items'),('module_item_kits','module_item_kits_desc',30,'item_kits'),('module_messages','module_messages_desc',98,'messages'),('module_mpesa','module_mpesa_desc',100,'mpesa'),('module_mpesaadmin','module_mpesa_admin',101,'mpesaadmin'),('module_office','module_office_desc',999,'office'),('module_receivings','module_receivings_desc',60,'receivings'),('module_reports','module_reports_desc',50,'reports'),('module_sales','module_sales_desc',70,'sales'),('module_suppliers','module_suppliers_desc',40,'suppliers'),('module_taxes','module_taxes_desc',105,'taxes');
/*!40000 ALTER TABLE `ospos_modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_mpesa_float`
--

DROP TABLE IF EXISTS `ospos_mpesa_float`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_mpesa_float` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop` int(11) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `date` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `shop_idx` (`shop`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_mpesa_float`
--

LOCK TABLES `ospos_mpesa_float` WRITE;
/*!40000 ALTER TABLE `ospos_mpesa_float` DISABLE KEYS */;
INSERT INTO `ospos_mpesa_float` VALUES (17,1,17000,'2018-05-13 22:28:04'),(18,2,17000,'2018-05-14 22:28:04'),(19,3,17000,'2018-05-17 22:28:04'),(20,4,17000,'2018-05-10 22:28:04'),(21,1,100000,'2018-05-15 22:33:53'),(22,2,100000,'2018-05-15 22:33:53'),(23,3,100000,'2018-05-15 22:33:53'),(24,4,100000,'2018-05-15 22:33:53');
/*!40000 ALTER TABLE `ospos_mpesa_float` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_mpesa_shop`
--

DROP TABLE IF EXISTS `ospos_mpesa_shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_mpesa_shop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_name` varchar(45) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `shop_code` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_mpesa_shop`
--

LOCK TABLES `ospos_mpesa_shop` WRITE;
/*!40000 ALTER TABLE `ospos_mpesa_shop` DISABLE KEYS */;
INSERT INTO `ospos_mpesa_shop` VALUES (1,'Shop 1',1,'2018-05-04 20:17:18','2018-05-12 12:11:28','123456'),(2,'Shop 2',1,'2018-05-04 20:17:18','2018-05-12 12:11:28','85274'),(3,'Shop 3',1,'2018-05-12 12:08:31','2018-05-12 12:11:28','96385'),(4,'Shop 4',1,'2018-05-12 12:08:31','2018-05-12 12:11:28','78912');
/*!40000 ALTER TABLE `ospos_mpesa_shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_mpesa_shop_user`
--

DROP TABLE IF EXISTS `ospos_mpesa_shop_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_mpesa_shop_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop` int(11) DEFAULT NULL,
  `user` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_mpesa_shop_user`
--

LOCK TABLES `ospos_mpesa_shop_user` WRITE;
/*!40000 ALTER TABLE `ospos_mpesa_shop_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `ospos_mpesa_shop_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_mpesa_transaction_type`
--

DROP TABLE IF EXISTS `ospos_mpesa_transaction_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_mpesa_transaction_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(45) DEFAULT NULL,
  `abbr` varchar(10) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='			';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_mpesa_transaction_type`
--

LOCK TABLES `ospos_mpesa_transaction_type` WRITE;
/*!40000 ALTER TABLE `ospos_mpesa_transaction_type` DISABLE KEYS */;
INSERT INTO `ospos_mpesa_transaction_type` VALUES (1,'Deposit','D',1,'2018-05-04 20:13:23','2018-05-04 20:14:38'),(2,'Withdrawal','W',1,'2018-05-04 20:13:23','2018-05-04 20:14:38');
/*!40000 ALTER TABLE `ospos_mpesa_transaction_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_mpesa_transactions`
--

DROP TABLE IF EXISTS `ospos_mpesa_transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_mpesa_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transac_type` int(11) DEFAULT NULL,
  `transact_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `cust_id` int(11) DEFAULT NULL,
  `phone_number` varchar(15) DEFAULT NULL,
  `shop` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `transaction_type_idx` (`transac_type`),
  KEY `user_id_idx` (`user_id`),
  KEY `shop_idx` (`shop`),
  CONSTRAINT `shop` FOREIGN KEY (`shop`) REFERENCES `ospos_mpesa_shop` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `transaction_type` FOREIGN KEY (`transac_type`) REFERENCES `ospos_mpesa_transaction_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `ospos_employees` (`person_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_mpesa_transactions`
--

LOCK TABLES `ospos_mpesa_transactions` WRITE;
/*!40000 ALTER TABLE `ospos_mpesa_transactions` DISABLE KEYS */;
INSERT INTO `ospos_mpesa_transactions` VALUES (15,1,'2018-05-14 12:30:03',2,14700,362541,'07154896',3),(29,1,'2018-05-14 12:30:03',2,1200,362541,'07154896',1),(30,2,'2018-05-14 12:30:03',2,1200,362541,'07154896',2),(31,1,'2018-05-14 12:30:03',2,1400,362541,'07154896',3),(32,2,'2018-05-14 12:30:03',2,15000,362541,'07154896',4),(33,1,'2018-05-14 12:30:03',2,16000,362541,'07154896',1),(34,2,'2018-05-14 12:30:03',2,17000,362541,'07154896',2),(35,1,'2018-05-14 12:30:03',2,100,362541,'07154896',3),(36,2,'2018-05-14 12:30:03',2,60,362541,'07154896',4),(37,1,'2018-05-14 12:30:03',2,180,362541,'07154896',1),(38,2,'2018-05-14 12:30:03',2,230,362541,'07154896',2),(39,1,'2018-05-14 12:30:03',2,250,362541,'07154896',3),(40,1,'2018-05-14 12:30:03',2,780,362541,'07154896',4);
/*!40000 ALTER TABLE `ospos_mpesa_transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_people`
--

DROP TABLE IF EXISTS `ospos_people`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_people` (
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `gender` int(1) DEFAULT NULL,
  `phone_number` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address_1` varchar(255) NOT NULL,
  `address_2` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zip` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `comments` text NOT NULL,
  `person_id` int(10) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`person_id`),
  KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_people`
--

LOCK TABLES `ospos_people` WRITE;
/*!40000 ALTER TABLE `ospos_people` DISABLE KEYS */;
INSERT INTO `ospos_people` VALUES ('John','Doe',NULL,'555-555-5555','changeme@example.com','Address 1','','','','','','',1),('Vincent','Bii',1,'','','','','','','','','',2);
/*!40000 ALTER TABLE `ospos_people` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_permissions`
--

DROP TABLE IF EXISTS `ospos_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_permissions` (
  `permission_id` varchar(255) NOT NULL,
  `module_id` varchar(255) NOT NULL,
  `location_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`permission_id`),
  KEY `module_id` (`module_id`),
  KEY `ospos_permissions_ibfk_2` (`location_id`),
  CONSTRAINT `ospos_permissions_ibfk_1` FOREIGN KEY (`module_id`) REFERENCES `ospos_modules` (`module_id`) ON DELETE CASCADE,
  CONSTRAINT `ospos_permissions_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `ospos_stock_locations` (`location_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_permissions`
--

LOCK TABLES `ospos_permissions` WRITE;
/*!40000 ALTER TABLE `ospos_permissions` DISABLE KEYS */;
INSERT INTO `ospos_permissions` VALUES ('config','config',NULL),('customers','customers',NULL),('employees','employees',NULL),('expenses','expenses',NULL),('expenses_categories','expenses_categories',NULL),('giftcards','giftcards',NULL),('home','home',NULL),('items','items',NULL),('items_stock','items',1),('item_kits','item_kits',NULL),('messages','messages',NULL),('mpesa','mpesa',NULL),('mpesaadmin','mpesaadmin',NULL),('office','office',NULL),('receivings','receivings',NULL),('receivings_stock','receivings',1),('reports','reports',NULL),('reports_categories','reports',NULL),('reports_customers','reports',NULL),('reports_discounts','reports',NULL),('reports_employees','reports',NULL),('reports_expenses_categories','reports',NULL),('reports_inventory','reports',NULL),('reports_items','reports',NULL),('reports_payments','reports',NULL),('reports_receivings','reports',NULL),('reports_sales','reports',NULL),('reports_suppliers','reports',NULL),('reports_taxes','reports',NULL),('sales','sales',NULL),('sales_delete','sales',NULL),('sales_stock','sales',1),('suppliers','suppliers',NULL),('taxes','taxes',NULL);
/*!40000 ALTER TABLE `ospos_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_receivings`
--

DROP TABLE IF EXISTS `ospos_receivings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_receivings` (
  `receiving_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `supplier_id` int(10) DEFAULT NULL,
  `employee_id` int(10) NOT NULL DEFAULT '0',
  `comment` text,
  `receiving_id` int(10) NOT NULL AUTO_INCREMENT,
  `payment_type` varchar(20) DEFAULT NULL,
  `reference` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`receiving_id`),
  KEY `supplier_id` (`supplier_id`),
  KEY `employee_id` (`employee_id`),
  KEY `reference` (`reference`),
  CONSTRAINT `ospos_receivings_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `ospos_employees` (`person_id`),
  CONSTRAINT `ospos_receivings_ibfk_2` FOREIGN KEY (`supplier_id`) REFERENCES `ospos_suppliers` (`person_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_receivings`
--

LOCK TABLES `ospos_receivings` WRITE;
/*!40000 ALTER TABLE `ospos_receivings` DISABLE KEYS */;
INSERT INTO `ospos_receivings` VALUES ('2018-04-26 04:18:49',NULL,2,'My Comments',1,'Cash',NULL);
/*!40000 ALTER TABLE `ospos_receivings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_receivings_items`
--

DROP TABLE IF EXISTS `ospos_receivings_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_receivings_items` (
  `receiving_id` int(10) NOT NULL DEFAULT '0',
  `item_id` int(10) NOT NULL DEFAULT '0',
  `description` varchar(30) DEFAULT NULL,
  `serialnumber` varchar(30) DEFAULT NULL,
  `line` int(3) NOT NULL,
  `quantity_purchased` decimal(15,3) NOT NULL DEFAULT '0.000',
  `item_cost_price` decimal(15,2) NOT NULL,
  `item_unit_price` decimal(15,2) NOT NULL,
  `discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_location` int(11) NOT NULL,
  `receiving_quantity` decimal(15,3) NOT NULL DEFAULT '1.000',
  PRIMARY KEY (`receiving_id`,`item_id`,`line`),
  KEY `item_id` (`item_id`),
  CONSTRAINT `ospos_receivings_items_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `ospos_items` (`item_id`),
  CONSTRAINT `ospos_receivings_items_ibfk_2` FOREIGN KEY (`receiving_id`) REFERENCES `ospos_receivings` (`receiving_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_receivings_items`
--

LOCK TABLES `ospos_receivings_items` WRITE;
/*!40000 ALTER TABLE `ospos_receivings_items` DISABLE KEYS */;
INSERT INTO `ospos_receivings_items` VALUES (1,2,'','',1,1.000,15000.00,15000.00,0.00,1,1.000);
/*!40000 ALTER TABLE `ospos_receivings_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_sales`
--

DROP TABLE IF EXISTS `ospos_sales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_sales` (
  `sale_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `customer_id` int(10) DEFAULT NULL,
  `employee_id` int(10) NOT NULL DEFAULT '0',
  `comment` text,
  `invoice_number` varchar(32) DEFAULT NULL,
  `quote_number` varchar(32) DEFAULT NULL,
  `sale_id` int(10) NOT NULL AUTO_INCREMENT,
  `sale_status` tinyint(2) NOT NULL DEFAULT '0',
  `dinner_table_id` int(11) DEFAULT NULL,
  `work_order_number` varchar(32) DEFAULT NULL,
  `sale_type` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sale_id`),
  UNIQUE KEY `invoice_number` (`invoice_number`),
  KEY `customer_id` (`customer_id`),
  KEY `employee_id` (`employee_id`),
  KEY `sale_time` (`sale_time`),
  KEY `dinner_table_id` (`dinner_table_id`),
  CONSTRAINT `ospos_sales_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `ospos_employees` (`person_id`),
  CONSTRAINT `ospos_sales_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `ospos_customers` (`person_id`),
  CONSTRAINT `ospos_sales_ibfk_3` FOREIGN KEY (`dinner_table_id`) REFERENCES `ospos_dinner_tables` (`dinner_table_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_sales`
--

LOCK TABLES `ospos_sales` WRITE;
/*!40000 ALTER TABLE `ospos_sales` DISABLE KEYS */;
INSERT INTO `ospos_sales` VALUES ('2018-04-26 04:20:37',NULL,2,'','0',NULL,1,0,NULL,NULL,0);
/*!40000 ALTER TABLE `ospos_sales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_sales_items`
--

DROP TABLE IF EXISTS `ospos_sales_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_sales_items` (
  `sale_id` int(10) NOT NULL DEFAULT '0',
  `item_id` int(10) NOT NULL DEFAULT '0',
  `description` varchar(255) DEFAULT NULL,
  `serialnumber` varchar(30) DEFAULT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `quantity_purchased` decimal(15,3) NOT NULL DEFAULT '0.000',
  `item_cost_price` decimal(15,2) NOT NULL,
  `item_unit_price` decimal(15,2) NOT NULL,
  `discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_location` int(11) NOT NULL,
  `print_option` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sale_id`,`item_id`,`line`),
  KEY `sale_id` (`sale_id`),
  KEY `item_id` (`item_id`),
  KEY `item_location` (`item_location`),
  CONSTRAINT `ospos_sales_items_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `ospos_items` (`item_id`),
  CONSTRAINT `ospos_sales_items_ibfk_2` FOREIGN KEY (`sale_id`) REFERENCES `ospos_sales` (`sale_id`),
  CONSTRAINT `ospos_sales_items_ibfk_3` FOREIGN KEY (`item_location`) REFERENCES `ospos_stock_locations` (`location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_sales_items`
--

LOCK TABLES `ospos_sales_items` WRITE;
/*!40000 ALTER TABLE `ospos_sales_items` DISABLE KEYS */;
INSERT INTO `ospos_sales_items` VALUES (1,2,'','',1,10.000,15000.00,25000.00,0.00,1,0);
/*!40000 ALTER TABLE `ospos_sales_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_sales_items_taxes`
--

DROP TABLE IF EXISTS `ospos_sales_items_taxes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_sales_items_taxes` (
  `sale_id` int(10) NOT NULL,
  `item_id` int(10) NOT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `percent` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `tax_type` tinyint(2) NOT NULL DEFAULT '0',
  `rounding_code` tinyint(2) NOT NULL DEFAULT '0',
  `cascade_tax` tinyint(2) NOT NULL DEFAULT '0',
  `cascade_sequence` tinyint(2) NOT NULL DEFAULT '0',
  `item_tax_amount` decimal(15,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`sale_id`,`item_id`,`line`,`name`,`percent`),
  KEY `sale_id` (`sale_id`),
  KEY `item_id` (`item_id`),
  CONSTRAINT `ospos_sales_items_taxes_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `ospos_sales_items` (`sale_id`),
  CONSTRAINT `ospos_sales_items_taxes_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `ospos_items` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_sales_items_taxes`
--

LOCK TABLES `ospos_sales_items_taxes` WRITE;
/*!40000 ALTER TABLE `ospos_sales_items_taxes` DISABLE KEYS */;
/*!40000 ALTER TABLE `ospos_sales_items_taxes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_sales_payments`
--

DROP TABLE IF EXISTS `ospos_sales_payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_sales_payments` (
  `sale_id` int(10) NOT NULL,
  `payment_type` varchar(40) NOT NULL,
  `payment_amount` decimal(15,2) NOT NULL,
  PRIMARY KEY (`sale_id`,`payment_type`),
  KEY `sale_id` (`sale_id`),
  CONSTRAINT `ospos_sales_payments_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `ospos_sales` (`sale_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_sales_payments`
--

LOCK TABLES `ospos_sales_payments` WRITE;
/*!40000 ALTER TABLE `ospos_sales_payments` DISABLE KEYS */;
INSERT INTO `ospos_sales_payments` VALUES (1,'Cash',250000.00);
/*!40000 ALTER TABLE `ospos_sales_payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_sales_reward_points`
--

DROP TABLE IF EXISTS `ospos_sales_reward_points`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_sales_reward_points` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sale_id` int(11) NOT NULL,
  `earned` float NOT NULL,
  `used` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sale_id` (`sale_id`),
  CONSTRAINT `ospos_sales_reward_points_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `ospos_sales` (`sale_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_sales_reward_points`
--

LOCK TABLES `ospos_sales_reward_points` WRITE;
/*!40000 ALTER TABLE `ospos_sales_reward_points` DISABLE KEYS */;
/*!40000 ALTER TABLE `ospos_sales_reward_points` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_sales_taxes`
--

DROP TABLE IF EXISTS `ospos_sales_taxes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_sales_taxes` (
  `sale_id` int(10) NOT NULL,
  `tax_type` smallint(2) NOT NULL,
  `tax_group` varchar(32) NOT NULL,
  `sale_tax_basis` decimal(15,4) NOT NULL,
  `sale_tax_amount` decimal(15,4) NOT NULL,
  `print_sequence` tinyint(2) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `tax_rate` decimal(15,4) NOT NULL,
  `sales_tax_code` varchar(32) NOT NULL DEFAULT '',
  `rounding_code` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sale_id`,`tax_type`,`tax_group`),
  KEY `print_sequence` (`sale_id`,`print_sequence`,`tax_type`,`tax_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_sales_taxes`
--

LOCK TABLES `ospos_sales_taxes` WRITE;
/*!40000 ALTER TABLE `ospos_sales_taxes` DISABLE KEYS */;
/*!40000 ALTER TABLE `ospos_sales_taxes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_sessions`
--

DROP TABLE IF EXISTS `ospos_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_sessions`
--

LOCK TABLES `ospos_sessions` WRITE;
/*!40000 ALTER TABLE `ospos_sessions` DISABLE KEYS */;
INSERT INTO `ospos_sessions` VALUES ('ojv5tepqbik6773mqkd30k3a5gm4sm7a','::1',1525605903,'__ci_last_regenerate|i:1525605610;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('5tsvq3ontqiimk7c2dqcvt4j5kepilfd','::1',1525606038,'__ci_last_regenerate|i:1525605914;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('28orf4jess1nqmto8k9cc5kp5b7quv9o','::1',1525606691,'__ci_last_regenerate|i:1525606690;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('9sjj6cgebsdaai3sppj9v1rdegfbmpg1','::1',1525607521,'__ci_last_regenerate|i:1525607234;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('le1o60s3gev8k9209p70cbe3hp7ub879','::1',1525616515,'__ci_last_regenerate|i:1525616261;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('esl71q6h0a62qcsvddbgl6347l6org94','::1',1525616882,'__ci_last_regenerate|i:1525616617;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('qsi3f4cilueaj1ggdp77tg82rapqh1hm','::1',1525616986,'__ci_last_regenerate|i:1525616947;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('2vss8nnv7tssf12eiqs580ciscihnrgu','::1',1525617730,'__ci_last_regenerate|i:1525617729;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('091a2tkbi7qqdmb6t3r2bn7vo1nipu6a','::1',1525618260,'__ci_last_regenerate|i:1525618117;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('1j546mi523rlmcmmnav67hd7pdss8b00','::1',1525618624,'__ci_last_regenerate|i:1525618438;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('5f6vo9il7ehl129tn1lplpehb98l5ghl','::1',1525619090,'__ci_last_regenerate|i:1525618831;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('v0na9oplfntpikb9v3pineed2mgets71','::1',1525619281,'__ci_last_regenerate|i:1525619141;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('69eud204e3bjf4dma4scvt3rtqau42ku','::1',1525619846,'__ci_last_regenerate|i:1525619642;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('sdvsapp3pv733h0c62a45u6sgvrne6u3','::1',1525620048,'__ci_last_regenerate|i:1525620048;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('88pl0rp1d5j6pel6ve8nvs7khpl5m3ku','::1',1525920165,'__ci_last_regenerate|i:1525920134;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('8fso34msg12pi5de2bkhgijqd8fed3n2','::1',1525921048,'__ci_last_regenerate|i:1525920792;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('qnl8s15q3dsl7pbkbs1toeicdgtq4ikg','::1',1525921227,'__ci_last_regenerate|i:1525921093;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('dbk6sdvipcdj17mvm9mvr7spdusopl4c','::1',1525921448,'__ci_last_regenerate|i:1525921399;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('f0fp4in0qts24gfdlls3tlfb72c69koa','::1',1525922036,'__ci_last_regenerate|i:1525922013;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('an83bn17mst3dgum2h9t8ijbe52f6ll1','::1',1525922486,'__ci_last_regenerate|i:1525922485;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('p7nnmocs40na7sbar4eu9f5qsd772l23','::1',1525923075,'__ci_last_regenerate|i:1525922860;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('97pofp7b1b21pb3eca7anpdafo9a1mk4','::1',1525924306,'__ci_last_regenerate|i:1525924042;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('9h58oa3gfu11uukkrii2pcfr9is58jtb','::1',1525925486,'__ci_last_regenerate|i:1525925486;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('e7c14i8n8uh6m5btaupdmgugq20l22lr','::1',1525970914,'__ci_last_regenerate|i:1525970911;session_sha1|s:7:\"4f5ad57\";'),('63adfq8f75fsbkk3rd2ukk41fha9ohph','::1',1525972225,'__ci_last_regenerate|i:1525971950;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('ob6drl290dqnvf404v5kam1nrpul8cp3','::1',1525972266,'__ci_last_regenerate|i:1525972265;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('hd1jcucjc6te7rv59qvdj20t0g3ig1rp','::1',1525973211,'__ci_last_regenerate|i:1525972995;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('vkkusqijn61p7mmcmln4ifi40gj4t42m','::1',1525973468,'__ci_last_regenerate|i:1525973319;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('pqncl0au6lmhrvt89aln8nge6l4qdp8k','::1',1525980348,'__ci_last_regenerate|i:1525980318;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('omjsoscd97ade7qvqu5b39f4m4o8i7ar','::1',1525982892,'__ci_last_regenerate|i:1525982730;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('5raelvpssi7hpmfhsrh87ckvk7e1jc3p','::1',1525983888,'__ci_last_regenerate|i:1525983606;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('8828gkqo3vg56s8b4gettv02sib39dk3','::1',1525984220,'__ci_last_regenerate|i:1525983922;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('5k5ld9bnasqrhus8pngaq8pujucctpis','::1',1525984480,'__ci_last_regenerate|i:1525984233;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('i0i4shr3pfg37dlfd9l0lrpip84tod1p','::1',1525984774,'__ci_last_regenerate|i:1525984545;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('0m0k75bc0t8b1fdrf6k7b2lpp3fee0f0','::1',1525985153,'__ci_last_regenerate|i:1525984881;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('rqterkn5q50u9unq3111qd74r0ahkig6','::1',1525985490,'__ci_last_regenerate|i:1525985203;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('ebtbja401dmthcs3dv4839onlhsloo36','::1',1525985795,'__ci_last_regenerate|i:1525985507;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('5oss0rchirischgsq0lgjgn0f5uhr54m','::1',1525986070,'__ci_last_regenerate|i:1525985816;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('k0m8076o66vke6nten4sreh26jbpnn6g','::1',1525986328,'__ci_last_regenerate|i:1525986185;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('g0gtpts1t0lc5kmaed1t2dqb6r18p4a6','::1',1525986881,'__ci_last_regenerate|i:1525986731;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('bg8u9tbg8214kled5qmh3f7gbup0qji2','::1',1526032511,'__ci_last_regenerate|i:1526032509;session_sha1|s:7:\"4f5ad57\";'),('u76njl3f8vm9c1ucn5e5akkbf5nhaa0m','::1',1526037184,'__ci_last_regenerate|i:1526037022;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('tklhpp302tpic5l25ps81ka1m91tob1f','::1',1526038264,'__ci_last_regenerate|i:1526038003;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('9fpccqi8ognm2oi34k89itg43v2cm1pv','::1',1526038567,'__ci_last_regenerate|i:1526038324;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('ace4dlvkc1k071o194kov223dut5fljr','::1',1526038731,'__ci_last_regenerate|i:1526038657;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('nk9999krljcglsrlqneleoo9ahvq5jj5','::1',1526039186,'__ci_last_regenerate|i:1526039033;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('51p2hssgd6c26g8kq80qbfc8tdcv0lut','::1',1526040925,'__ci_last_regenerate|i:1526040730;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('g916q9ffper1lsqkarf89osfveoomo31','::1',1526041285,'__ci_last_regenerate|i:1526041090;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('j5hq5ecfmp2mlu8pr7sn8shoflli7a41','::1',1526041832,'__ci_last_regenerate|i:1526041565;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('8v60o4ph91vobnl84b2ukveopoqb3h0l','::1',1526042160,'__ci_last_regenerate|i:1526041869;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('r6f18ict4fi5m0m95omclhsd6r184u3d','::1',1526042175,'__ci_last_regenerate|i:1526042174;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('ptpriuk5iaep8brq9084o41p5pgea6g2','::1',1526043092,'__ci_last_regenerate|i:1526042807;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('pkhid88osrl8le6jcjg6ip86tc080r4h','::1',1526043446,'__ci_last_regenerate|i:1526043146;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('4sqiaj8khbp0lvg5kenni9mvmu3sbc04','::1',1526043629,'__ci_last_regenerate|i:1526043451;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('lqj83ku4bkstnvkm4rltlp0ehn1gf0t7','::1',1526046451,'__ci_last_regenerate|i:1526046183;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('ilr40m307dgfiu2lef02c3gg0l6i4ka0','::1',1526046494,'__ci_last_regenerate|i:1526046494;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('jatomiknqlt9husuq08j73orlf0u5h7i','::1',1526046989,'__ci_last_regenerate|i:1526046987;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('hmi7d6akhtvovv8je9dmqpj4edtqpr56','::1',1526047539,'__ci_last_regenerate|i:1526047538;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('cjqv0nvtfvfepqdnj1l2dq0naprs6sad','::1',1526048406,'__ci_last_regenerate|i:1526048382;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";'),('1ma4s5r8g9pg9fa52ch3injoqnfrri83','::1',1526109740,'__ci_last_regenerate|i:1526109728;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('cohsjlumc53qrjo36l6ftfrfkuuc01cm','::1',1526111039,'__ci_last_regenerate|i:1526110757;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('07famuevh9go0lod4qigf73og1nva4g2','::1',1526111317,'__ci_last_regenerate|i:1526111059;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('1gl8be5avpc9sihq3mvpp8qoj6qhus8d','::1',1526111676,'__ci_last_regenerate|i:1526111391;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('jmkld6ckkkedl5ho4ti150r3tt05rs8u','::1',1526111811,'__ci_last_regenerate|i:1526111810;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('lnf5a5oqgm8vs7dnjatj4ojmfbvkbecs','::1',1526112186,'__ci_last_regenerate|i:1526112185;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('uctrp2ki0ukl54h9ge4tq2v8jd04mvh0','::1',1526112777,'__ci_last_regenerate|i:1526112543;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('9tueddg6ilbbd40e69jce3sdv3v8uqcd','::1',1526113100,'__ci_last_regenerate|i:1526112892;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('73e6e39k2bm6g6s41rii7sb0mo2ta5sv','::1',1526113560,'__ci_last_regenerate|i:1526113280;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('h6nus1mgfb19hasq394mrp1vea8fajrj','::1',1526113844,'__ci_last_regenerate|i:1526113583;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('63pnvv1c747c1d2f6fs5d76447vb13kj','::1',1526114238,'__ci_last_regenerate|i:1526114073;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('jcp5crrqt6ikpbcq5v3uk1ui8id2lvp9','::1',1526114872,'__ci_last_regenerate|i:1526114858;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('fmn60arpqfh32h52k04ca9oaejhfj597','::1',1526116403,'__ci_last_regenerate|i:1526116401;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('ojeinrbk00fbpnkaldvegkj2s7ja8f52','::1',1526116951,'__ci_last_regenerate|i:1526116883;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('3f2rr8agph1jel212vp71kjbnbsj909p','::1',1526118567,'__ci_last_regenerate|i:1526118295;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('er027cfo6bq5533mo9djk1ms66qbilgs','::1',1526119727,'__ci_last_regenerate|i:1526119725;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('ln5p30f2o7e566o3gt53oepea2ubs7si','::1',1526120470,'__ci_last_regenerate|i:1526120227;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('l39ng2co5ljaluehtvc56oo23p6lbh2p','::1',1526120857,'__ci_last_regenerate|i:1526120629;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('1fgngsbrafu35glh814vkmb7mg7o7djv','::1',1526121261,'__ci_last_regenerate|i:1526120988;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('mc74pt7eqdhsthg39qckh65oci1iil2s','::1',1526121435,'__ci_last_regenerate|i:1526121340;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('il1fr4l2kbohud29vvvnv2up1v585mrq','::1',1526122604,'__ci_last_regenerate|i:1526122324;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('oqk82tkdsn0h9lmej3b475gsn0gadg17','::1',1526122700,'__ci_last_regenerate|i:1526122642;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('lo6f4tl1nem548e14i3sfotbo87mcnsi','::1',1526123037,'__ci_last_regenerate|i:1526122984;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('bgu1m3d66vjv5p98c8f5l5dg5aujnvfo','::1',1526123970,'__ci_last_regenerate|i:1526123752;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('fdgkefbur04i2dfr160i03amlfvnl0re','::1',1526125788,'__ci_last_regenerate|i:1526125756;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('02i36cv7vuslkim11js4n0jeh7lq7suj','::1',1526126069,'__ci_last_regenerate|i:1526126069;'),('7ct30qu8q3b1r722c7k0ctebo6nv5slt','::1',1526126070,'__ci_last_regenerate|i:1526126070;session_sha1|s:7:\"4f5ad57\";'),('jvns0gvoc1jm7nh7mavc00ufq974ok1o','::1',1526126072,'__ci_last_regenerate|i:1526126069;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('js6ck5hm05406vbcfdi92s8a58jvgahp','::1',1526126480,'__ci_last_regenerate|i:1526126480;'),('uuolf27afbksp2svs7fmab3sc5mca87d','::1',1526126480,'__ci_last_regenerate|i:1526126480;session_sha1|s:7:\"4f5ad57\";'),('u1c93hjn06a1hsnm9pse370a1o9tg247','::1',1526126700,'__ci_last_regenerate|i:1526126480;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('r3jk7kklv0p6db627eh6dhbn2ut29300','::1',1526126561,'__ci_last_regenerate|i:1526126561;'),('7srr6sdbu7drinfa639id1398jkdnlkc','::1',1526126562,'__ci_last_regenerate|i:1526126561;session_sha1|s:7:\"4f5ad57\";'),('4nu4kaed0s0svug42eredn54rih30jnf','::1',1526127062,'__ci_last_regenerate|i:1526126782;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('h01m5r20l3229g5m93bpbc9kuc8e5mii','::1',1526127473,'__ci_last_regenerate|i:1526127090;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('hmgkld9p9176ifo6k68n4q2uv1j7skj6','::1',1526127634,'__ci_last_regenerate|i:1526127473;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('qcc7s5evqqhfn2dkj38q0df1u41dc0h6','::1',1526127903,'__ci_last_regenerate|i:1526127860;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('9gjlclcf0vs571nt153coeu2onhqgk33','::1',1526128612,'__ci_last_regenerate|i:1526128607;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('i4v3g43iejfepmejj1hqp98aplouhv6p','::1',1526128978,'__ci_last_regenerate|i:1526128976;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('1japoq3nb2crs357k3p5bsn02igh6ofo','::1',1526129742,'__ci_last_regenerate|i:1526129539;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('cbs62g21fk9226c7s40iroqsvgg8c08e','::1',1526130192,'__ci_last_regenerate|i:1526129892;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('vo5l8oiiii9uo01v733o3315ufi69a3g','::1',1526130823,'__ci_last_regenerate|i:1526130680;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('0332m19ipoi25bi1rbp52usaetc3uurt','::1',1526131730,'__ci_last_regenerate|i:1526131479;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('80a3h30ig8q92oh14dt93ls688ilvje5','::1',1526131899,'__ci_last_regenerate|i:1526131886;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('cg5ogafn7nuqjio6836hk4h777ng1u5h','::1',1526217166,'__ci_last_regenerate|i:1526216984;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('j4kersfoudu52saq7lpavjrfpatqn858','::1',1526218462,'__ci_last_regenerate|i:1526218165;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('29d340cmbpnq8gmn2t5bsm177l9hiovr','::1',1526218727,'__ci_last_regenerate|i:1526218473;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('e2700g2qmpg3rf0e0s2d9s10dvge7nrs','::1',1526219125,'__ci_last_regenerate|i:1526218873;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('1mivsahg4aip5f8c5q2hfs06k5kh469s','::1',1526219711,'__ci_last_regenerate|i:1526219216;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('o0qrotomv0llt0hc4ocfe3icqk7ufg93','::1',1526219753,'__ci_last_regenerate|i:1526219715;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('l4809fgscvudvhu75pj5krta9brohbtk','::1',1526220306,'__ci_last_regenerate|i:1526220042;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('hkldu5kljii1cedoohb4cn4kb7g27sp0','::1',1526220635,'__ci_last_regenerate|i:1526220350;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('akgvle32bshh8f2f5s5ed7p8et4jg4ev','::1',1526221005,'__ci_last_regenerate|i:1526220692;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('o5hp6oookmhdkpbhbdjdj6e7kguinijp','::1',1526221187,'__ci_last_regenerate|i:1526221125;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('5l6uitvhr23028tqfbcku4ge4hjceq00','::1',1526221704,'__ci_last_regenerate|i:1526221480;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('e9ftusgfuucrc3k83b68fi57ggm6mmsm','::1',1526222219,'__ci_last_regenerate|i:1526221921;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('sl3aqqfd3shodktr5qvj2nvkkgg1h310','::1',1526222371,'__ci_last_regenerate|i:1526222237;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('ql4lu1nuab0djji359a9cuhne69qkpob','::1',1526223656,'__ci_last_regenerate|i:1526222639;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('vv5jguhtf5iu9gk9e3ilqm0eug3dc8t8','::1',1526223583,'__ci_last_regenerate|i:1526223578;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('gpeqnqiomi8go5l2i5mqirufvkantuk8','::1',1526223862,'__ci_last_regenerate|i:1526223726;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('pran9mj1nc4geicjg1v0ntahljgjpl1m','::1',1526237884,'session_sha1|s:7:\"4f5ad57\";__ci_last_regenerate|i:1526237599;person_id|s:1:\"2\";'),('s6gs21h9hv8qn35go36eho54bbku84sn','::1',1526237943,'session_sha1|s:7:\"4f5ad57\";__ci_last_regenerate|i:1526237907;person_id|s:1:\"2\";'),('ommn38k4bj02o26p8k8881be9dqbfabq','::1',1526272695,'__ci_last_regenerate|i:1526272688;session_sha1|s:7:\"4f5ad57\";'),('mklimdj45g3o3e7muo04bnje732km88h','::1',1526273451,'__ci_last_regenerate|i:1526273312;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('o5sru7g3sfefueu31fkl7t2u9pkds5ta','::1',1526273905,'__ci_last_regenerate|i:1526273757;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('cb2rpqv24reb3l57kohdt2vpcr6evrfd','::1',1526275321,'__ci_last_regenerate|i:1526275028;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";recv_cart|a:0:{}recv_mode|s:7:\"receive\";recv_supplier|i:-1;sales_cart|a:0:{}sales_customer|i:-1;sales_mode|s:4:\"sale\";sales_location|s:1:\"1\";sales_payments|a:0:{}cash_mode|i:1;cash_rounding|i:0;sales_invoice_number|N;'),('s6h2s9i6r203p88j8h5b7qe3jj6l9hak','::1',1526275363,'__ci_last_regenerate|i:1526275362;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";item_location|s:1:\"1\";recv_cart|a:0:{}recv_mode|s:7:\"receive\";recv_supplier|i:-1;sales_cart|a:0:{}sales_customer|i:-1;sales_mode|s:4:\"sale\";sales_location|s:1:\"1\";sales_payments|a:0:{}cash_mode|i:1;cash_rounding|i:0;sales_invoice_number|N;'),('da9jglahutdlpj81v3ittmjdkrdogtll','::1',1526290433,'__ci_last_regenerate|i:1526290170;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('log64ug2i1coh8731hf0cke1au7o7ole','::1',1526294204,'__ci_last_regenerate|i:1526294037;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('5om33vj0s8b4j8brpjr2gh0sahqcmkms','::1',1526295157,'__ci_last_regenerate|i:1526295133;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('bje3p8crk2pfuhic3it5lmh75e7kgg20','::1',1526370320,'__ci_last_regenerate|i:1526370300;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('bpm51jift8ge5f2l3r8g4408dtfbjj32','::1',1526371026,'__ci_last_regenerate|i:1526370762;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('c9hohcmg709lad51h8sp3l228988l55k','::1',1526371078,'__ci_last_regenerate|i:1526371077;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('es89a6hratcf1atus4v28m1lsjjm4jl2','::1',1526371548,'__ci_last_regenerate|i:1526371395;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('pfgb8b8booc3o4fg3q2supl7ocf8nse8','::1',1526372409,'__ci_last_regenerate|i:1526372255;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('oh0u6h5di40dg5ct5ednokeeishv67od','::1',1526372815,'__ci_last_regenerate|i:1526372569;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('11lddgp000u5pe8bldk0gesk9b4d28at','::1',1526373184,'__ci_last_regenerate|i:1526372911;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('vpk3nc8cogjp10rt8of29cftp6fl86ii','::1',1526373514,'__ci_last_regenerate|i:1526373219;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('libtcl0pbsm4q19fleeb6suricn0ppkn','::1',1526373841,'__ci_last_regenerate|i:1526373547;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('tn43cd5k0ssdsm40gqv9ggdhp4tfllgg','::1',1526374178,'__ci_last_regenerate|i:1526373909;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('hc0uh1ofvihtennq8tt92kuc9ubiid0l','::1',1526374612,'__ci_last_regenerate|i:1526374315;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('gek8i0tkluf97ctcr5encjnafj8sgac4','::1',1526374960,'__ci_last_regenerate|i:1526374668;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('g0jisoucs1ut00kumbt0ckm9oav07bo3','::1',1526375325,'__ci_last_regenerate|i:1526375035;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('bf3k7i1i7qbrfcgln57167lho82t1fcd','::1',1526375638,'__ci_last_regenerate|i:1526375338;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('cprnmdu4b22b20e4d2vqebpe49n67ifl','::1',1526377402,'__ci_last_regenerate|i:1526377399;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('7th7cf9mdaviiuu3cgljph3ic8fptmf2','::1',1526379202,'__ci_last_regenerate|i:1526379184;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('logfnpk747ndb8ekr6gkev4sjek6r7hn','::1',1526379877,'__ci_last_regenerate|i:1526379596;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('kkpq8ecqnafvvsc9b1u112phkjtraemv','::1',1526379936,'__ci_last_regenerate|i:1526379936;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('80cp4c12shp6ubf36g02l4chnqbgcs41','::1',1526380292,'__ci_last_regenerate|i:1526380260;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('v90ndt5c0vjdroeuc2jfkc2rmkda0m8b','::1',1526381130,'__ci_last_regenerate|i:1526381129;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('qo7kaker4u3uitfashugqm4l8nl15gqv','::1',1526387105,'__ci_last_regenerate|i:1526387087;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('mfmbaqhe333brj1p4o2ku8nj0qdq6lp1','::1',1526387767,'__ci_last_regenerate|i:1526387526;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('4b670kmkb44cnrjvs8mhhjkjn4rmn3vb','::1',1526387927,'__ci_last_regenerate|i:1526387925;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('ba176nn6h0uqmocno5hcuqt2c8gnehvs','::1',1526388582,'__ci_last_regenerate|i:1526388286;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('a0753fgfljv2mo6mo613l0kbgoe7gf99','::1',1526388945,'__ci_last_regenerate|i:1526388707;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('ah55s4j1pfrp9vgjis71j1p63n70o8am','::1',1526389469,'__ci_last_regenerate|i:1526389170;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('ci9e3dubi0pr5dlfr9us4469puroq2e1','::1',1526389503,'__ci_last_regenerate|i:1526389492;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('7h11ut8pqes8bi1tqgnrg2vdfi6ac10q','::1',1526390215,'__ci_last_regenerate|i:1526389928;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('cc1937un4lq3kc02jappg9lh6meibiqt','::1',1526390521,'__ci_last_regenerate|i:1526390250;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('bjrevmucu56cv5ctaf1ohs74o06buegq','::1',1526390978,'__ci_last_regenerate|i:1526390976;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('nhie4184ljrbtkbsagj5jltsmnlaarfs','::1',1526392025,'__ci_last_regenerate|i:1526391749;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('pfloelpntm5nlh9tqjpclrqg0q0kcgio','::1',1526393953,'__ci_last_regenerate|i:1526393737;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('10ijva7v5jti78skf5qoctr3ka5lprki','::1',1526394331,'__ci_last_regenerate|i:1526394065;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('h9gs8jsqim47b644s3onr9gecf4203ei','::1',1526394660,'__ci_last_regenerate|i:1526394371;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('uln8mpoefeoqsp71lucet52chd5v03ck','::1',1526394899,'__ci_last_regenerate|i:1526394703;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('hh4lqpkb0mrob7mao9hd4a7imnnvbnda','::1',1526395561,'__ci_last_regenerate|i:1526395307;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('qurhelr91hiicoc2rch1sni0euiesdqc','::1',1526395800,'__ci_last_regenerate|i:1526395631;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('3mf1vih5o0ibqftpk72g3g8191vk90mn','::1',1526396102,'__ci_last_regenerate|i:1526395952;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('3mnk8ia6ap5ba1ce7fiq11u2dca88fnk','::1',1526396807,'__ci_last_regenerate|i:1526396511;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('519m79tnfh10sna7snhr8tcfmeqskeee','::1',1526397331,'__ci_last_regenerate|i:1526397045;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('q608034mq4mis128lfef8p5jkt5pnu5g','::1',1526397506,'__ci_last_regenerate|i:1526397353;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('nfgd3bh3il515impudocjqdl587sm5oi','::1',1526398119,'__ci_last_regenerate|i:1526398040;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('sek12gtovul6slr7imch0j03ngki402m','::1',1526411644,'__ci_last_regenerate|i:1526411396;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('s5ttn81k8c39b2dl6osq0cn1c5ctbdav','::1',1526412033,'__ci_last_regenerate|i:1526411765;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('tcqngihltoupcj2vc41bq8jpkc6f4gei','::1',1526412487,'__ci_last_regenerate|i:1526412318;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('otuktrvnd7t92j5tcak1ggsgvrqt6m3h','::1',1526412833,'__ci_last_regenerate|i:1526412661;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('ss4jpu2dlfka5hdjpogob87itlod1pm4','::1',1526413237,'__ci_last_regenerate|i:1526412966;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('j6ulhg4c40rfgjn0t8gupsqbgo9n0t94','::1',1526413356,'__ci_last_regenerate|i:1526413355;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('1ll7bsoutbov03kin1mjie3fm9ndebl6','::1',1526414067,'__ci_last_regenerate|i:1526413804;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('rqvl4pbipdoi14bculg5jl5dv7j5b76g','::1',1526414468,'__ci_last_regenerate|i:1526414164;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('r7kjkuj6d4hq2r6lr9u75rdhlt1p2m1t','::1',1526414795,'__ci_last_regenerate|i:1526414520;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('2e51m4tad191uu421jk6qn026jvkkuod','::1',1526415212,'__ci_last_regenerate|i:1526414917;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('prgnes8vbbdp6p7orkghkqu7hhopo9gj','::1',1526415472,'__ci_last_regenerate|i:1526415225;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('eogqj4agcbt8teil6929gpc0v2o40tmv','::1',1526416334,'__ci_last_regenerate|i:1526416074;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('sudc09crnpsgag5il4j8ngvh6oqldd00','::1',1526416761,'__ci_last_regenerate|i:1526416517;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('bf694hjoa3ek9v4m3k3f7dk1i1vq56fv','::1',1526417258,'__ci_last_regenerate|i:1526416972;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('pvdekogvd69mb8nrvkh9pmc2msv22arq','::1',1526417579,'__ci_last_regenerate|i:1526417285;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('puimipen656hf925bdi499eeuqt6e1jb','::1',1526417910,'__ci_last_regenerate|i:1526417612;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('e8gmubh0lpok49d70u607n5k16e1nckq','::1',1526418202,'__ci_last_regenerate|i:1526417913;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('sqmnblgco9m084brmjjfciqpgjsfi38j','::1',1526418363,'__ci_last_regenerate|i:1526418268;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('ts6c05raokk0ccvu8h9o0a77tu9kjlg0','::1',1526419124,'__ci_last_regenerate|i:1526418849;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('2dad3j7dq0tps9e2v2pi0pskvjll8k4p','::1',1526419172,'__ci_last_regenerate|i:1526419170;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";'),('eo86jjecuae8en6egonna2h7hfrmdh9f','::1',1526419870,'__ci_last_regenerate|i:1526419575;session_sha1|s:7:\"4f5ad57\";person_id|s:1:\"2\";');
/*!40000 ALTER TABLE `ospos_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_stock_locations`
--

DROP TABLE IF EXISTS `ospos_stock_locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_stock_locations` (
  `location_id` int(11) NOT NULL AUTO_INCREMENT,
  `location_name` varchar(255) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`location_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_stock_locations`
--

LOCK TABLES `ospos_stock_locations` WRITE;
/*!40000 ALTER TABLE `ospos_stock_locations` DISABLE KEYS */;
INSERT INTO `ospos_stock_locations` VALUES (1,'stock',0),(2,'stock',0),(3,'stock',0);
/*!40000 ALTER TABLE `ospos_stock_locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_suppliers`
--

DROP TABLE IF EXISTS `ospos_suppliers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_suppliers` (
  `person_id` int(10) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `agency_name` varchar(255) NOT NULL,
  `account_number` varchar(255) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `account_number` (`account_number`),
  KEY `person_id` (`person_id`),
  CONSTRAINT `ospos_suppliers_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `ospos_people` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_suppliers`
--

LOCK TABLES `ospos_suppliers` WRITE;
/*!40000 ALTER TABLE `ospos_suppliers` DISABLE KEYS */;
/*!40000 ALTER TABLE `ospos_suppliers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_tax_categories`
--

DROP TABLE IF EXISTS `ospos_tax_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_tax_categories` (
  `tax_category_id` int(10) NOT NULL AUTO_INCREMENT,
  `tax_category` varchar(32) NOT NULL,
  `tax_group_sequence` tinyint(2) NOT NULL,
  PRIMARY KEY (`tax_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_tax_categories`
--

LOCK TABLES `ospos_tax_categories` WRITE;
/*!40000 ALTER TABLE `ospos_tax_categories` DISABLE KEYS */;
INSERT INTO `ospos_tax_categories` VALUES (1,'Standard',10),(2,'Service',12),(3,'Alcohol',11),(4,'Standard',10),(5,'Service',12),(6,'Alcohol',11),(7,'Standard',10),(8,'Service',12),(9,'Alcohol',11);
/*!40000 ALTER TABLE `ospos_tax_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_tax_code_rates`
--

DROP TABLE IF EXISTS `ospos_tax_code_rates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_tax_code_rates` (
  `rate_tax_code` varchar(32) NOT NULL,
  `rate_tax_category_id` int(10) NOT NULL,
  `tax_rate` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `rounding_code` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`rate_tax_code`,`rate_tax_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_tax_code_rates`
--

LOCK TABLES `ospos_tax_code_rates` WRITE;
/*!40000 ALTER TABLE `ospos_tax_code_rates` DISABLE KEYS */;
/*!40000 ALTER TABLE `ospos_tax_code_rates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_tax_codes`
--

DROP TABLE IF EXISTS `ospos_tax_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_tax_codes` (
  `tax_code` varchar(32) NOT NULL,
  `tax_code_name` varchar(255) NOT NULL DEFAULT '',
  `tax_code_type` tinyint(2) NOT NULL DEFAULT '0',
  `city` varchar(255) NOT NULL DEFAULT '',
  `state` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`tax_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_tax_codes`
--

LOCK TABLES `ospos_tax_codes` WRITE;
/*!40000 ALTER TABLE `ospos_tax_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `ospos_tax_codes` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-16  0:33:09
